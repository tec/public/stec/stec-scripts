#!/usr/bin/env python3
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="stec",
    version="1.0.0",
    author="Andreas Biri",
    author_email="abiri@ethz.ch",
    description="STeC testing scripts",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ethz.ch/tec/public/stec/stec-scripts",
    packages=setuptools.find_packages(),
    install_requires=[
        'setuptools',
        'pandas',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
)
