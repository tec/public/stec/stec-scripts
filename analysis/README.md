# STeC Testing & Data Analysis Scripts

Scripts for automatic testing of STeC on FlockLab as well as to automatically analyse FlockLab tests and statistical data from GSN.

## Interface

```
usage: stec-flocklab.py [-h] [-a INT] [-d INT] [-np] [-n INT] [-e INT] [-g INT] [-bs INT] [-max INT] [-bsimg INT] [-p STRING] [-t] [-tp STRING]

usage: stec-flocklab.py [options]

optional arguments:
-h, --help                           Show this help message and exit
-a INT, --abort-test INT             Aborts the currently running test with the given ID.
-d INT, --delete-test INT            Deletes the test with the given ID.
-np, --no-powertracing               Disables power tracing for this test.
-n INT, --nodes INT                  Number of nodes exclusive BaseStation (default: 2).
-e INT, --events INT                 Number of scheduled events (default: 1).
-g INT, --gap INT                    Gap between events [ms] (default: 500).
-bs INT, --basestation INT           ID of node that will be the BaseStation (default: 1).
-max INT, --maximum-points INT       Limit number of data points to be fetched from GSN (default: unlimited).
-bsimg INT, --basestation-image INT  FlockLab image for BaseStation (default: None).
-p STRING, --path STRING             Path to .elf files to be uploaded (default: './').
-t, --trace                          Use a trace file to simulate events.
-tp STRING, --trace-path STRING      Path to .log file of trace.
```

```
usage: stec-analysis.py [-h] [-v] [-p STRING] [-s] [-l INT] [-r] [-a] [-g]

usage: stec-analysis.py [options]

optional arguments:
  -h, --help                    Show this help message and exit
  -v, --verify-tests            Verify locally scheduled tests.
  -p STRING, --path STRING      Path to .log file of tests.
  -s, --statistics              Display statistics on co-detections.
  -l INT, --codet-length INT    Length of a co-detection [ms].
  -r, --include-rain            Include rainy days in statistics.
  -a, --auto-extend             Automatically extend the event duration if it is still running.
  -g, --geophone-data           Extract Geophone features from log files.
```

```
usage: stec-utils.py [-h] [-g] [-p STRING] [-c INT] [-e INT] [-d INT] [-i INT] [-s]

usage: stec-utils.py [options]

optional arguments:
  -h, --help                    Show this help message and exit
  -g, --generate-trace          Generate new trace.
  -p STRING, --path STRING      Path to files (default: './').
  -c INT, --nr-codets INT       Number of generated co-detections.
  -e INT, --nr-evts INT         Number of generated events per co-detection.
  -d INT, --prop-delay INT      Maximal propagation delay of co-detections [ms] (default: 100ms).
  -i INT, --event-interval INT  Average event interval [s] (default: 20s).
  -s, --show-metrics            Displays the information of a stored metric.
```

## Further resources

### GSN web interface
- [A decade of detailed observations in steep bedrock permafrost at Matterhorn Hörnligrad (Zermatt, CH) - Weber et al. 19](https://doi.org/10.3929/ethz-b-000323342)
- [GSN Web-interface @ GitHub](https://github.com/LSIR/gsn/wiki/Web-Interface#multidata)
