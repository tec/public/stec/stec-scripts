#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Analysis script for the STeC project
#
# Author: abiri
# Date:   03.11.21

import logging
import configparser
import sys
import datetime as dt
import flocklab
from ast      import literal_eval
from argparse import ArgumentParser
from os.path  import isfile, isdir, abspath

sys.path.append("../")  # FIXME: Work around if not built as a package
from data_management.data_manager    import DataManager
from data_management.data_processing import DataProcessor

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# Default params for command line arguments that are mandatory
DEFAULT_CONFIG_FILE = '../stec.conf'
DEFAULT_LOG_LEVEL   = 'INFO'

DEPLOYMENT = 'dirruhorn'

VS_ACOUSTIC_METADATA = '_dpp_geophone_acq__conv'
VS_ACOUSTIC_DATA     = '_dpp_geophone_adcData__conv'

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class STECAnalyser:

    def __init__(self):

        self.LOG_FORMAT       = None
        self.LOG_DATE_FORMAT  = None
        self.LOG_FILE_NAME    = None
        self.LOG_FILE_LEVEL   = None
        self.LOG_STREAM_LEVEL = None
        self.DATA_START_TIME  = dt.datetime.strptime("01/01/2017", "%d/%m/%Y")  # type: dt.datetime
        self.DATA_END_TIME    = dt.datetime.strptime("01/01/2020", "%d/%m/%Y")  # type: dt.datetime
        self.DATES_EXCLUDED   = None

        # Load config
        self.load_config(DEFAULT_CONFIG_FILE)

        self._DataMgr  = DataManager(deployment=DEPLOYMENT, config_file=DEFAULT_CONFIG_FILE, project_name='stec', start_time=self.DATA_START_TIME, end_time=self.DATA_END_TIME)
        self._DataProc = DataProcessor(config_file=DEFAULT_CONFIG_FILE, project_name='stec')

    def load_config(self, config_file=None):

        if config_file is None:
            config_file = DEFAULT_CONFIG_FILE
            logging.info('Using default configuration file: %s' % (config_file,))

        if not isfile(config_file):
            logging.warning('Config file (%s) not found' % (config_file,))
        else:
            config_file = abspath(config_file)

            # Read config file for other options
            config = configparser.ConfigParser()
            config.optionxform = str  # Case sensitive
            config.read(config_file)

            section_common  = 'stec'
            section_special = 'stec-bins'
            try:
                # Read options from config
                for name, value in (config.items(section_common) + config.items(section_special)):
                    value = value.strip()
                    if value != '':
                        if   name == 'log_format':
                            self.LOG_FORMAT = str(value)
                        elif name == 'log_date_format':
                            self.LOG_DATE_FORMAT = str(value)
                        elif name == 'log_file_name':
                            self.LOG_FILE_NAME = str(value)
                        elif name == 'log_file_level':
                            self.LOG_FILE_LEVEL = getattr(logging, value.upper())
                        elif name == 'log_stream_level':
                            self.LOG_STREAM_LEVEL = getattr(logging, value.upper())
                        elif name == 'data_start_time':
                            self.DATA_START_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'data_end_time':
                            self.DATA_END_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'dates_excluded':
                            self.DATES_EXCLUDED = literal_eval(value)
                        else:
                            logging.warning('Unknown config option in section [%s]: %s' % (section_common + "/" + section_special, name,))
            except configparser.NoSectionError:
                raise TypeError('No [%s] section specified in %s' % (section_common + "/" + section_special, config_file,))

    def verify_tests(self, log_path):

        # Get all locally scheduled tests
        logs = self._DataMgr.get_logged_tests(log_path)

        # Fetch corresponding tests
        fl       = flocklab.Flocklab()
        test_ids = logs['test_id']
        download_directory = './'

        for test_id in test_ids:
            if isdir(download_directory + str(test_id)):
                logging.info('Skipping test %d as already downloaded' % (test_id,))
            else:
                # Fetch test from FlockLab - catch FlockLab errors directly to not affect other tests
                try:
                    if 'Successfully downloaded & extracted:' in fl.getResults(test_id):
                        logging.info('Successfully downloaded test %d' % (test_id,))
                    else:
                        logging.warning('Failed to download test %d' % (test_id,))

                except flocklab.flocklab.FlocklabError as exception:
                    logging.warning('Experienced FlockLab error for test %d: %s' % (test_id, exception))

        # Extract metrics
        metrics_tracing, metrics_serial, metrics_power = self._DataMgr.extract_test_metrics(test_ids, download_directory)

        # Compare metrics
        self._DataProc.compare_metrics(test_input=logs, test_output=[metrics_tracing, metrics_serial, metrics_power])

        # Extract metrics for comparison with simulation
        if metrics_power is not None:
            self._DataMgr.store_metrics(test_input=logs, test_output=[metrics_tracing, metrics_serial, metrics_power], path=download_directory)

        return True

    def fetch_statistics(self, codet_length_ms=0, include_rain=False, auto_extend=False):

        # Create URL including conditions
        fields = 'device_id,start_time,end_time,generation_time,trg_duration'
        url    = self._DataMgr.assemble_gsn_url(VS_ACOUSTIC_METADATA, fields)

        # Get dates to be excluded (usually due to maintenance)
        excluded_dates = self.DATES_EXCLUDED

        # Fetch data
        try:
            df = self._DataMgr.fetch_csv_data(url)

            # Mark and filter days with precipitation
            prec_dates = self._DataMgr.fetch_rain_dates(include_gruengarten=True, include_breithorn=False, include_grabengufer=False)
            self._DataProc.mark_rain_dates(df, prec_dates)

            if not include_rain:
                if len(prec_dates):
                    excluded_dates = excluded_dates + prec_dates
                    logging.info("Added %d days to list of excepted dates due to precipitation" % (len(prec_dates)))
        except Exception as ex_acoustic:
            logging.error("Could not fetch CSV data from path:\n%s\n\n%s" % (url, ex_acoustic,))
            return

        logging.debug("Received data from URL:\n%s" % (url,))

        if codet_length_ms > 0:
            events = self._DataProc.compute_event_stats(df, dates_excluded=excluded_dates, propagation_ms=codet_length_ms, auto_extend_evt=auto_extend)
            codets = self._DataProc.find_codetections(df,   dates_excluded=excluded_dates, propagation_ms=codet_length_ms, auto_extend_evt=auto_extend)
        else:
            events = self._DataProc.compute_event_stats(df, dates_excluded=excluded_dates, auto_extend_evt=auto_extend)
            codets = self._DataProc.find_codetections(df,   dates_excluded=excluded_dates, auto_extend_evt=auto_extend)

        # Print statistics
        self._DataProc.print_event_stats(events)
        self._DataProc.print_codet_stats(codets, force_print=True)
        self._DataProc.print_meas_stats( codets, force_print=True)

    def extract_geophone_features(self, path):
        if path is None:
            raise ValueError('Path to files must not be None')
        elif not isdir(path):
            raise ValueError('Path \'%s\' must point to a directory' % (path,))

        # Get data
        [df_acq, df_com, df_evts, df_health] = self._DataMgr.extract_geophone_data(path)

        # Store dataframes
        file_path = path + '/geophone_acq.pkl'
        df_acq.to_pickle(file_path)

        file_path = path + '/geophone_com.pkl'
        df_com.to_pickle(file_path)

        file_path = path + '/geophone_events.pkl'
        df_evts.to_pickle(file_path)

        file_path = path + '/geophone_health.pkl'
        df_health.to_pickle(file_path)

        # Store acquisitions as CSV
        df_acq_converted = self._DataMgr.convert_geophone_data(df_acq)
        file_path = path + '/cache_etz_dpp_geophone_acq_logs__conv_20201019_20210517.csv'
        df_acq_converted.to_csv(file_path)

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Create STECAnalyser to store parameters
        analyser = STECAnalyser()

        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-v', '--verify-tests', dest='verify', action='store_true',
                            help='Verify locally scheduled tests.')
        parser.add_argument('-p', '--path', type=str, dest='path',
                            help='Path to .log file of tests.', metavar='STRING')
        parser.add_argument('-s', '--statistics', dest='stats', action='store_true',
                            help='Display statistics on co-detections.')
        parser.add_argument('-l', '--codet-length', type=int, dest='codet_length_ms', default=0,
                            help='Length of a co-detection [ms].', metavar='INT')
        parser.add_argument('-r', '--include-rain', dest='include_rain', action='store_true',
                            help='Include rainy days in statistics.')
        parser.add_argument('-a', '--auto-extend', dest='auto_extend_evt', action='store_true',
                            help='Automatically extend the event duration if it is still running.')
        parser.add_argument('-g', '--geophone-data', dest='extract_geophone_features', action='store_true',
                            help='Extract Geophone features from log files.')
        args = parser.parse_args()

        # Check arguments
        if args.path is not None and not isinstance(args.path, str):
            raise TypeError('Invalid path to .log files: %s' % (args.path,))
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Initialize logger
    if analyser.LOG_FORMAT is None:
        analyser.LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    if analyser.LOG_DATE_FORMAT is None:
        analyser.LOG_DATE_FORMAT = '%H:%M:%S'

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=analyser.LOG_FORMAT, datefmt=analyser.LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    if analyser.LOG_FILE_LEVEL is not None:
        # Log root logger to file (FileHandler)
        if analyser.LOG_FILE_NAME is not None:
            handler = logging.FileHandler(filename=analyser.LOG_FILE_NAME)
            handler.setLevel(analyser.LOG_FILE_LEVEL)
            handler.setFormatter(formatter)
            root.addHandler(handler)
        else:
            logging.warning('Log file name is not defined, skipping logging...')
    if analyser.LOG_STREAM_LEVEL is not None:
        # Pipe root logger to stdout (StreamHandler)
        handler = logging.StreamHandler()
        handler.setLevel(analyser.LOG_STREAM_LEVEL)
        handler.setFormatter(formatter)
        root.addHandler(handler)

    # Print debug options
    logging.debug('STeC Analysis CLI options:')
    for arg in vars(args):
        logging.debug(' {}:\t {}'.format(arg, getattr(args, arg) or ''))

    logging.info("STeC Analysis script is running...")

    try:
        # Check arguments
        if args.verify:
            # Fetch all scheduled tests and verify them
            analyser.verify_tests(args.path)
        if args.stats:
            analyser.fetch_statistics(codet_length_ms=args.codet_length_ms, include_rain=args.include_rain, auto_extend=args.auto_extend_evt)
        if args.extract_geophone_features:
            analyser.extract_geophone_features(args.path)
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
