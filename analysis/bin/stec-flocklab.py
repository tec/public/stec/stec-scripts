#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Flocklab script for the STeC project
#
# Author: abiri
# Date:   03.11.21

import logging
import configparser
import sys
import datetime as dt
import flocklab
from ast import literal_eval
from argparse import ArgumentParser
from os.path import isfile, abspath

sys.path.append("../")  # FIXME: Work around if not built as a package
from data_management.data_manager    import DataManager
from data_management.data_processing import DataProcessor

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# Default params for command line arguments that are mandatory
DEFAULT_CONFIG_FILE = '../stec.conf'
DEFAULT_LOG_LEVEL   = 'INFO'

# Nodes 15, 17 and 25 are available but external nodes and not in FSK range; 30 is on ETZ roof
FL_NODE_IDS          = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32]
FL_NODE_REMOTE_IDS   = [15, 17, 25, 30]
FL_NODE_EXCLUDED_IDS = [6, 9, 10, 13]

DEPLOYMENT = 'dirruhorn'

VS_ACOUSTIC_METADATA = '_dpp_geophone_acq__conv'
VS_ACOUSTIC_DATA     = '_dpp_geophone_adcData__conv'

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class STECFlocklabManager:

    def __init__(self):

        self.DEFAULT_NR_NODES   = 2               # type: int
        self.DEFAULT_NR_EVTS    = 1               # type: int
        self.DEFAULT_EVT_GAP_MS = 5*1000          # type: int
        self.DEFAULT_BS_ID      = FL_NODE_IDS[0]  # type: int
        self.EVT_GAP_MS_MIN     = 0               # type: int
        self.EVT_GAP_MS_MAX     = 3600*1000       # type: int
        self.LOG_FORMAT         = None
        self.LOG_DATE_FORMAT    = None
        self.LOG_FILE_NAME      = None
        self.LOG_FILE_LEVEL     = None
        self.LOG_STREAM_LEVEL   = None
        self.DATA_START_TIME    = dt.datetime.strptime("01/01/2017", "%d/%m/%Y")  # type: dt.datetime
        self.DATA_END_TIME      = dt.datetime.strptime("01/10/2020", "%d/%m/%Y")  # type: dt.datetime
        self.DATES_EXCLUDED     = None

        self.nr_nodes           = None
        self.nr_evts            = None
        self.evt_gap_ms         = None
        self.path               = './'  # type: str
        self.basestation_id     = None
        self.basestation_image  = None
        self.max_nr_points      = None

        # Load config
        self.load_config(DEFAULT_CONFIG_FILE)

        self._DataMgr  = DataManager(deployment=DEPLOYMENT, config_file=DEFAULT_CONFIG_FILE, project_name='stec', start_time=self.DATA_START_TIME, end_time=self.DATA_END_TIME)
        self._DataProc = DataProcessor(config_file=DEFAULT_CONFIG_FILE, project_name='stec')
        self._codet_sample = None

    def load_config(self, config_file=None):

        if config_file is None:
            config_file = DEFAULT_CONFIG_FILE
            logging.info('Using default configuration file: %s' % (config_file,))

        if not isfile(config_file):
            logging.warning('Config file (%s) not found' % (config_file,))
        else:
            config_file = abspath(config_file)

            # Read config file for other options
            config = configparser.ConfigParser()
            config.optionxform = str  # Case sensitive
            config.read(config_file)

            section_common  = 'stec'
            section_special = 'stec-bins'
            try:
                # Read options from config
                for name, value in (config.items(section_common) + config.items(section_special)):
                    value = value.strip()
                    if value != '':
                        if   name == 'default_nr_nodes':
                            self.DEFAULT_NR_NODES = int(value)
                        elif name == 'default_nr_events':
                            self.DEFAULT_NR_EVTS = int(value)
                        elif name == 'default_evt_gap':
                            self.DEFAULT_EVT_GAP_MS = int(value)
                        elif name == 'default_bs_id':
                            self.DEFAULT_BS_ID = int(value)
                        elif name == 'evt_gap_ms_min':
                            self.EVT_GAP_MS_MIN = int(value)
                        elif name == 'evt_gap_ms_max':
                            self.EVT_GAP_MS_MAX = int(value)
                        elif name == 'log_format':
                            self.LOG_FORMAT = str(value)
                        elif name == 'log_date_format':
                            self.LOG_DATE_FORMAT = str(value)
                        elif name == 'log_file_name':
                            self.LOG_FILE_NAME = str(value)
                        elif name == 'log_file_level':
                            self.LOG_FILE_LEVEL = getattr(logging, value.upper())
                        elif name == 'log_stream_level':
                            self.LOG_STREAM_LEVEL = getattr(logging, value.upper())
                        elif name == 'data_start_time':
                            self.DATA_START_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'data_end_time':
                            self.DATA_END_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'dates_excluded':
                            self.DATES_EXCLUDED = literal_eval(value)
                        else:
                            logging.warning('Unknown config option in section [%s]: %s' % (section_common + "/" + section_special, name,))
            except configparser.NoSectionError:
                raise TypeError('No [%s] section specified in %s' % (section_common + "/" + section_special, config_file,))

    @staticmethod
    def is_valid_FL_id(node_id):
        return node_id in FL_NODE_IDS

    def load_codetections(self, path):

        # Load full trace from file
        trace = self._DataMgr.load_codetection_trace(path)

        # Reduce to ordered time trace
        trace = self._DataMgr.reduce_to_time_trace(trace)

        return trace

    def fetch_acoustic_data(self):

        # Create URL including conditions
        fields = 'device_id,start_time,end_time,generation_time,trg_duration'
        url = self._DataMgr.assemble_gsn_url(VS_ACOUSTIC_METADATA, fields, max_nr_points=self.max_nr_points)

        # Fetch data
        try:
            df = self._DataMgr.fetch_csv_data(url)
        except Exception as ex_acoustic:
            logging.error("Could not fetch CSV data from path:\n%s\n\n%s" % (url, ex_acoustic,))
            return

        logging.info("Received data from URL:\n%s" % (url,))

        # Print statistics
        self._DataProc.print_df_stats(df)

        return df

    def fetch_codetections(self, df, include_rain=True):

        # Filter days with precipitation
        excluded_dates = self.DATES_EXCLUDED
        if not include_rain:
            prec_dates = self._DataMgr.fetch_rain_dates()
            if len(prec_dates):
                excluded_dates = self.DATES_EXCLUDED + prec_dates
                logging.info("Added %d days to list of excepted dates due to precipitation" % (len(prec_dates)))

        return self._DataProc.find_codetections(df, dates_excluded=excluded_dates)

    def generate_xml(self, codet_list, powertracing_enabled=True, random_sampling=True):

        # Fetch subset
        self._codet_sample = DataProcessor.gather_codetection_sample(codet_list=codet_list, max_nr_events=self.nr_evts, min_nr_of_nodes=self.nr_nodes, max_nr_of_nodes=(len(FL_NODE_IDS)-1), random=random_sampling)

        if len(self._codet_sample) < self.nr_evts:
            logging.warning("Tried to schedule %u events, but only %u co-detections with sufficient nodes (>= %u) were available" % (self.nr_evts, len(self._codet_sample), self.nr_nodes,))

        # Verify that FlockLab IDs are up-to-date and available
        fl = flocklab.Flocklab()
        available_ids = set(fl.getObsIds('DPP2LoRa')) - set(FL_NODE_EXCLUDED_IDS)

        if not set(FL_NODE_IDS).issubset(available_ids):
            logging.warning("Not all given FlockLab IDs are available; expected %s, but available IDs are %s" % (str(sorted(FL_NODE_IDS)), str(sorted(available_ids)),))
            unavailable_ids = []
            for fl_id in FL_NODE_IDS:
                if fl_id not in available_ids:
                    unavailable_ids.append(fl_id)

            for fl_id in unavailable_ids:
                FL_NODE_IDS.remove(fl_id)  # Requires separate loop, as otherwise iterator in first loop jumps the next following ID
            logging.warning("Removed IDs %s from the set of available nodes" % (str(unavailable_ids),))

        # Create XML
        self._DataMgr.create_FL_xml(codet_list=self._codet_sample, eligible_ids=FL_NODE_IDS, bs_id=self.basestation_id, evt_gap_t=self.evt_gap_ms, path=self.path, bs_img=self.basestation_image, powertracing_enabled=powertracing_enabled)

    def upload_xml(self):

        # Access fl-tools functions
        fl = flocklab.Flocklab()

        # Validate XML
        ret = fl.xmlValidate(DataManager.get_test_xml())
        if 'validated correctly' not in ret:
            raise ValueError('Invalid FlockLab XML file: %s' % (ret,))
        else:
            logging.debug("XML validated correctly")

        # Upload XML
        ret = fl.createTestWithInfo(DataManager.get_test_xml())
        if 'was successfully added' not in ret:
            raise ValueError('Could not upload XML to FlockLab: %s' % (ret,))
        else:
            logging.info(ret.replace('was successfully added and is scheduled to start', 'starts'))

            # Log test
            ret = ret.split(' ')
            test_nr   = ret[1]
            test_date = ret[11] + ' ' + ret[12]
            self._DataMgr.log_test(','.join([test_nr, test_date, str(self._codet_sample)]))

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Create STECFlocklabManager to store parameters
        fl_manager = STECFlocklabManager()

        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-a', '--abort-test', type=int, dest='abort_id',
                            help='Aborts the currently running test with the given ID.', metavar='INT')
        parser.add_argument('-d', '--delete-test', type=int, dest='delete_id',
                            help='Deletes the test with the given ID.', metavar='INT')
        parser.add_argument('-np', '--no-powertracing', dest='power', action='store_false',
                            help='Disables power tracing for this test.')
        parser.add_argument('-n', '--nodes', type=int, dest='nr_nodes', default=fl_manager.DEFAULT_NR_NODES,
                            help='Number of nodes excluding BaseStation (default: %d).' % (fl_manager.DEFAULT_NR_NODES,), metavar='INT')
        parser.add_argument('-e', '--events', type=int, dest='nr_evts', default=fl_manager.DEFAULT_NR_EVTS,
                            help='Number of scheduled events (default: %d).' % (fl_manager.DEFAULT_NR_EVTS,), metavar='INT')
        parser.add_argument('-g', '--gap', type=int, dest='evt_gap_ms', default=fl_manager.DEFAULT_EVT_GAP_MS,
                            help='Gap between events [ms] (default: %d).' % (fl_manager.DEFAULT_EVT_GAP_MS,), metavar='INT')
        parser.add_argument('-bs', '--basestation', type=int, dest='basestation_id', default=fl_manager.DEFAULT_BS_ID,
                            help='ID of node that will be the BaseStation (default: %d).' % (fl_manager.DEFAULT_BS_ID,), metavar='INT')
        parser.add_argument('-max', '--maximum-points', type=int, dest='max_nr_data_points', default=0,
                            help='Limit number of data points to be fetched from GSN (default: unlimited).', metavar='INT')
        parser.add_argument('-bsimg', '--basestation-image', type=int, dest='basestation_image', default=None,
                            help='FlockLab image for BaseStation (default: None).', metavar='INT')
        parser.add_argument('-p', '--path', type=str, dest='path', default='./',
                            help='Path to .elf files to be uploaded (default: \'./\').', metavar='STRING')
        parser.add_argument('-t', '--trace', dest='use_trace', action='store_true',
                            help='Use a trace file to simulate events.')
        parser.add_argument('-tp', '--trace-path', type=str, dest='trace_path',
                            help='Path to .log file of trace.', metavar='STRING')
        args = parser.parse_args()

        # Check arguments
        if args.abort_id is not None and not isinstance(args.abort_id, int):
            raise TypeError('Invalid test number to abort: %s' % (args.abort_id,))
        if not isinstance(args.nr_nodes, int) or (args.nr_nodes > (len(FL_NODE_IDS)-1)):
            raise ValueError('Invalid number of nodes: %s' % (args.nr_nodes,))
        else:
            fl_manager.nr_nodes = args.nr_nodes
        if not isinstance(args.nr_evts, int):
            raise TypeError('Invalid number of events: %s' % (args.nr_evts,))
        else:
            fl_manager.nr_evts = args.nr_evts
        if not isinstance(args.evt_gap_ms, int) or (args.evt_gap_ms < fl_manager.EVT_GAP_MS_MIN) or (args.evt_gap_ms > fl_manager.EVT_GAP_MS_MAX):
            raise ValueError('Invalid gap between events: %s' % (args.evt_gap_ms,))
        else:
            fl_manager.evt_gap_ms = args.evt_gap_ms
        if not isinstance(args.basestation_id, int) or not STECFlocklabManager.is_valid_FL_id(args.basestation_id):
            raise ValueError('Invalid Basestation ID: %s' % (args.basestation_id,))
        else:
            fl_manager.basestation_id = args.basestation_id
        if not isinstance(args.max_nr_data_points, int) and args.max_nr_data_points is not None:
            raise TypeError('Invalid maximal number of data points: %s' % (args.max_nr_data_points,))
        elif args.max_nr_data_points == 0 and fl_manager.max_nr_points is None:
            fl_manager.max_nr_points = None
        elif args.max_nr_data_points > 0:
            fl_manager.max_nr_points = args.max_nr_data_points
        if not isinstance(args.basestation_image, int) and args.basestation_image is not None:
            raise TypeError('Invalid FL image ID for BaseStation: %s' % (args.basestation_image,))
        else:
            fl_manager.basestation_image = args.basestation_image
        if not isinstance(args.path, str):
            raise TypeError('Invalid path to .elf files: %s' % (args.path,))
        else:
            fl_manager.path = args.path
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Initialize logger
    if fl_manager.LOG_FORMAT is None:
        fl_manager.LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    if fl_manager.LOG_DATE_FORMAT is None:
        fl_manager.LOG_DATE_FORMAT = '%H:%M:%S'

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=fl_manager.LOG_FORMAT, datefmt=fl_manager.LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    if fl_manager.LOG_FILE_LEVEL is not None:
        # Log root logger to file (FileHandler)
        if fl_manager.LOG_FILE_NAME is not None:
            handler = logging.FileHandler(filename=fl_manager.LOG_FILE_NAME)
            handler.setLevel(fl_manager.LOG_FILE_LEVEL)
            handler.setFormatter(formatter)
            root.addHandler(handler)
        else:
            logging.warning('Log file name is not defined, skipping logging...')
    if fl_manager.LOG_STREAM_LEVEL is not None:
        # Pipe root logger to stdout (StreamHandler)
        handler = logging.StreamHandler()
        handler.setLevel(fl_manager.LOG_STREAM_LEVEL)
        handler.setFormatter(formatter)
        root.addHandler(handler)

    # Print debug options
    logging.debug('STeC Flocklab CLI options:')
    for arg in vars(args):
        logging.debug(' {}:\t {}'.format(arg, getattr(args, arg) or ''))

    logging.debug("STeC FlockLab script is running...")

    try:
        # Abort test
        if args.abort_id is not None:
            logging.info(flocklab.Flocklab().abortTest(args.abort_id))
            sys.exit(1)
        elif args.delete_id is not None:
            logging.info(flocklab.Flocklab().deleteTest(args.delete_id))
            sys.exit(1)

        # Prepare test
        if args.use_trace:
            logging.debug("Loading co-detection trace...")
            codetections = fl_manager.load_codetections(args.trace_path)
            logging.info("Loaded co-detection trace")
        else:
            logging.debug("Fetching acoustic data...")
            data = fl_manager.fetch_acoustic_data()
            logging.info("Fetched acoustic data")

            # Get co-detections
            logging.debug("Analyzing co-detections...")
            codetections = fl_manager.fetch_codetections(data)
            logging.info("Detected co-detections")

        # Create text XML
        logging.debug("Generating FlockLab XML for testing...")
        if args.power is not None:
            fl_manager.generate_xml(codetections, powertracing_enabled=args.power, random_sampling=not args.use_trace)
        else:
            fl_manager.generate_xml(codetections, random_sampling=not args.use_trace)
        logging.info("Created FlockLab XML")

        # Send test to FL
        logging.debug("Sending XML to server...")
        fl_manager.upload_xml()
        logging.debug("Sent XML to server")

        # Finishing up
        logging.debug("Exiting STeC script")
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
