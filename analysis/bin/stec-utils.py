#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Util script for the STeC project
#
# Author: abiri
# Date:   03.11.21

import logging
import configparser
import sys
import datetime as dt
import flocklab
from ast import literal_eval
from argparse import ArgumentParser
from os.path import isfile, isdir, abspath, dirname

sys.path.append('../')  # FIXME: Work around if not built as a package
sys.path.append('../../system_model')
from data_management.data_manager    import DataManager
from data_management.data_processing import DataProcessor
from des.des_simulator               import Simulator

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# Default params for command line arguments that are mandatory
DEFAULT_CONFIG_FILE = '../stec.conf'
DEFAULT_LOG_LEVEL   = 'INFO'

FILE_DIR = dirname(__file__)

DEPLOYMENT = 'dirruhorn'

VS_ACOUSTIC_METADATA = '_dpp_geophone_acq__conv'
VS_ACOUSTIC_DATA     = '_dpp_geophone_adcData__conv'

# Global constants
MAX_DELAY_MS   = 100
EVT_INTERVAL_S = 20

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class STECUtils:

    def __init__(self):

        self.LOG_FORMAT       = None
        self.LOG_DATE_FORMAT  = None
        self.LOG_STREAM_LEVEL = None
        self.DATA_START_TIME  = dt.datetime.strptime("01/01/2017", "%d/%m/%Y")  # type: dt.datetime
        self.DATA_END_TIME    = dt.datetime.strptime("01/01/2020", "%d/%m/%Y")  # type: dt.datetime
        self.DATES_EXCLUDED   = None

        # Load config
        self.load_config(DEFAULT_CONFIG_FILE)

        self._DataMgr  = DataManager(deployment=DEPLOYMENT, config_file=DEFAULT_CONFIG_FILE, project_name='stec', start_time=self.DATA_START_TIME, end_time=self.DATA_END_TIME)
        self._DataProc = DataProcessor(config_file=DEFAULT_CONFIG_FILE, project_name='stec')

    def load_config(self, config_file=None):

        if config_file is None:
            config_file = DEFAULT_CONFIG_FILE
            logging.info('Using default configuration file: %s' % (config_file,))

        if not isfile(config_file):
            logging.warning('Config file (%s) not found' % (config_file,))
        else:
            config_file = abspath(config_file)

            # Read config file for other options
            config = configparser.ConfigParser()
            config.optionxform = str  # Case sensitive
            config.read(config_file)

            section_common  = 'stec'
            section_special = 'stec-bins'
            try:
                # Read options from config
                for name, value in (config.items(section_common) + config.items(section_special)):
                    value = value.strip()
                    if value != '':
                        if   name == 'log_format':
                            self.LOG_FORMAT = str(value)
                        elif name == 'log_date_format':
                            self.LOG_DATE_FORMAT = str(value)
                        elif name == 'log_file_name':
                            pass  # No logging to file for this script
                        elif name == 'log_stream_level':
                            self.LOG_STREAM_LEVEL = getattr(logging, value.upper())
                        elif name == 'data_start_time':
                            self.DATA_START_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'data_end_time':
                            self.DATA_END_TIME = dt.datetime.strptime(value, "%d/%m/%Y")
                        elif name == 'dates_excluded':
                            self.DATES_EXCLUDED = literal_eval(value)
                        else:
                            logging.warning('Unknown config option in section [%s]: %s' % (section_common + "/" + section_special, name,))
            except configparser.NoSectionError:
                raise TypeError('No [%s] section specified in %s' % (section_common + "/" + section_special, config_file,))

    def generate_trace(self, codets, evts, prop_delay_ms, evt_interval_s):

        # Generate trace
        trace = self._DataMgr.generate_codetection_trace(nr_codets=codets, nr_evts=evts, max_prop_delay_ms=prop_delay_ms, inter_arrival_s=evt_interval_s, interval_variation_percent=25)

        # Store trace
        file_name = self._DataMgr.generate_trace_postfix(codets, evts, prop_delay_ms if (prop_delay_ms != MAX_DELAY_MS) else None, evt_interval_s if (evt_interval_s != EVT_INTERVAL_S) else None)
        file_path = self._DataMgr.store_codetection_trace(trace, file_name)

        logging.info('Generated new trace file at %s' % (file_path,))

    @staticmethod
    def show_metrics(path):

        if   'ebc'   in path:
            df = Simulator.load_metrics(load_ebc=True,   postfix=path[len('ebc'):])
        elif 'stec'  in path:
            df = Simulator.load_metrics(load_ebc=True,   postfix=path[len('stec'):])
        elif 'elwb'  in path:
            df = Simulator.load_metrics(load_elwb=True,  postfix=path[len('elwb'):])
        elif 'aloha' in path:
            df = Simulator.load_metrics(load_aloha=True, postfix=path[len('aloha'):])
        else:
            logging.error('Invalid node type in path %s' % (path,))
            return

        if df is not None:
            Simulator.print_node_statistics(df.to_dict('records'))

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Create STECAnalyser to store parameters
        utils = STECUtils()

        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-g', '--generate-trace', dest='generate_trace', action='store_true',
                            help='Generate new trace.')
        parser.add_argument('-p', '--path', type=str, dest='path',
                            help='Path to files (default: \'%s/\').' % (FILE_DIR,), metavar='STRING')
        parser.add_argument('-c', '--nr-codets', type=int, dest='nr_codets', default=0,
                            help='Number of generated co-detections.', metavar='INT')
        parser.add_argument('-e', '--nr-evts', type=int, dest='nr_evts', default=0,
                            help='Number of generated events per co-detection.', metavar='INT')
        parser.add_argument('-d', '--prop-delay', type=int, dest='prop_delay', default=MAX_DELAY_MS,
                            help='Maximal propagation delay of co-detections [ms] (default: %ims).' % (MAX_DELAY_MS,), metavar='INT')
        parser.add_argument('-i', '--event-interval', type=int, dest='evt_interval', default=EVT_INTERVAL_S,
                            help='Average event interval [s] (default: %is).' % (EVT_INTERVAL_S,), metavar='INT')
        parser.add_argument('-s', '--show-metrics', dest='show_metrics', action='store_true',
                            help='Displays the information of a stored metric.')
        cli_args = parser.parse_args()

        # Check arguments
        if cli_args.path is not None and not isinstance(cli_args.path, str):
            raise TypeError('Invalid path to files: %s' % (cli_args.path,))
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Initialize logger
    if utils.LOG_FORMAT is None:
        utils.LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    if utils.LOG_DATE_FORMAT is None:
        utils.LOG_DATE_FORMAT = '%H:%M:%S'

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=utils.LOG_FORMAT, datefmt=utils.LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    if utils.LOG_STREAM_LEVEL is not None:
        # Pipe root logger to stdout (StreamHandler)
        handler = logging.StreamHandler()
        handler.setLevel(utils.LOG_STREAM_LEVEL)
        handler.setFormatter(formatter)
        root.addHandler(handler)

    # Print debug options
    logging.debug('STeC Util CLI options:')
    for arg in vars(cli_args):
        logging.debug(' {}:\t {}'.format(arg, getattr(cli_args, arg) or ''))

    logging.info("STeC Util script is running...")

    try:
        # Check arguments
        if cli_args.generate_trace:
            # Generate a new trace file
            utils.generate_trace(codets=cli_args.nr_codets, evts=cli_args.nr_evts, prop_delay_ms=cli_args.prop_delay, evt_interval_s=cli_args.evt_interval)
        elif cli_args.show_metrics:
            # Print metric information
            utils.show_metrics(path=cli_args.path)
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
