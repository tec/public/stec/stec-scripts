#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Analysis script for the STeC project
# Notice: This script has been used with an earlier version of the protocol and its energy and latency metrics potentially diverge from the ones obtained with the DES.
#
# Author: abiri
# Date:   03.11.21

import pandas as pd
import logging
import sys
from argparse import ArgumentParser
from math import ceil, floor, log2, comb

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

DEFAULT_LOG_LEVEL = 'INFO'

LOG_FORMAT      = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FORMAT = '%H:%M:%S'

DEFAULT_NR_NODES         = 10
DEFAULT_SF               = 7
DEFAULT_BATTERY_SIZE_mAh = 13000

BAT_VOLTAGE_V = 3.65

# HW parameters
LPM_mW = 0.010485

DS_ENERGY_RX_mJ = 0.076748
DS_ENERGY_TX_mJ = 0.172722

MAX_EVENT_DURATION_MS = 1 * 1000

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class EBCSimulator:

    @classmethod
    def simulate_event_duration(cls, spreading_factor, nr_nodes, nr_ids=32, co_detection=2):
        # Test 1922 and 4543 unless otherwise specified

        # Staggered Wakeup
        sw_duration_transition_ms = 13.17

        sw_duration_ms = 100

        fsk_duration_glossy_ms = 2.27

        sw_duration_ms = sw_duration_ms + 2 * fsk_duration_glossy_ms + sw_duration_transition_ms


        # Leader Election
        le_duration_transition_ms = 7.17

        le_duration_slot_ms = 28.00

        nr_bits = ceil(log2(nr_ids))

        if nr_ids < nr_nodes:
            logging.warning('Simulating for a maximum number of %i IDs, but currently have %i nodes' % (nr_ids, nr_nodes,))

        le_duration_ms = nr_bits * le_duration_slot_ms + le_duration_transition_ms


        # Data aggregation
        da_duration_transition_ms = MAX_EVENT_DURATION_MS

        da_duration_sched_ms = 34.81
        da_duration_slot_ms  = 25.03

        da_duration_ms = da_duration_sched_ms + da_duration_slot_ms * nr_ids + da_duration_transition_ms


        # Reporting
        # Note: assumption that no commands are sent
        rp_duration_transition_ms = 23.78

        nr_retransmissions = 0

        if spreading_factor == 7:
            rp_duration_overhead_ms = 149.26
            rp_duration_per_node_ms = 18.82

            ad_duration_transition_ms = 1599.61  # 7809.64ms if using SF10 as fallback
        elif spreading_factor == 10:
            rp_duration_overhead_ms = 796.51
            rp_duration_per_node_ms = 111.00

            ad_duration_transition_ms = 7809.80
        elif spreading_factor == 11:
            rp_duration_overhead_ms = 1652.69
            rp_duration_per_node_ms = 220.23

            ad_duration_transition_ms = 16275.48
        else:
            raise ValueError('Unsupported spreading factor %i' % (spreading_factor,))

        rp_duration_slot_ms = rp_duration_overhead_ms + nr_nodes * rp_duration_per_node_ms
        rp_duration_ms      = (1 + nr_retransmissions) * rp_duration_slot_ms + rp_duration_transition_ms

        # Filter if no co-detection
        if nr_nodes < co_detection:
            logging.info('Ignoring reporting duration (%10.2f ms) for network of %i nodes, as below co-detection threshold of %i' % (rp_duration_ms, nr_nodes, co_detection,))
            rp_duration_ms = 0


        # Ack Distribution
        # Note: assumption that no commands are sent
        ad_duration_transition_ms = ad_duration_transition_ms - rp_duration_ms  # Reporting and AD transition are in parallel

        ad_duration_comm_ms = 62.09

        ad_duration_ms = ad_duration_comm_ms + ad_duration_transition_ms

        # Filter if single network
        if nr_nodes <= 1:
            ad_duration_ms = 0


        # Comparison
        tot_duration_ms = sw_duration_ms + le_duration_ms + da_duration_ms + rp_duration_ms + ad_duration_ms

        logging.info(
            '\nSW: %10.2f ms (%5.2f %%)\nLE: %10.2f ms (%5.2f %%)\nDA: %10.2f ms (%5.2f %%)\nRP: %10.2f ms (%5.2f %%)\nAD: %10.2f ms (%5.2f %%)\nDuration per event: %8.2f ms' % (
                sw_duration_ms, (100 * sw_duration_ms / tot_duration_ms),
                le_duration_ms, (100 * le_duration_ms / tot_duration_ms),
                da_duration_ms, (100 * da_duration_ms / tot_duration_ms),
                rp_duration_ms, (100 * rp_duration_ms / tot_duration_ms),
                ad_duration_ms, (100 * ad_duration_ms / tot_duration_ms),
                tot_duration_ms,))

        return [tot_duration_ms, sw_duration_ms, le_duration_ms, da_duration_ms, rp_duration_ms, ad_duration_ms]

    @classmethod
    def simulate_event_energy(cls, spreading_factor, nr_nodes, nr_ids=32, co_detection=2):
        # Test 1922 and 4543 unless otherwise specified

        # Staggered Wakeup
        sw_duration_s = 0.1

        fsk_rx_mW        = 32.297273
        fsk_tx_glossy_mJ = 0.172722

        sw_energy_mJ = sw_duration_s * fsk_rx_mW + 2 * fsk_tx_glossy_mJ


        # Leader Election
        le_energy_slot_rx_mJ      = 1.267412
        le_energy_slot_retrans_mJ = 0.892855
        le_energy_slot_tx_mJ      = 0.810045

        le_bits_set_percent = 75
        nr_bits             = ceil(log2(nr_ids))

        if nr_ids < nr_nodes:
            logging.warning('Simulating for a maximum number of %i IDs, but currently have %i nodes' % (nr_ids, nr_nodes,))

        le_energy_mJ = le_bits_set_percent / 100 * nr_bits * le_energy_slot_tx_mJ + (100 - le_bits_set_percent) / 100 * nr_bits * le_energy_slot_rx_mJ


        # Data aggregation
        da_energy_sched_retrans_mJ = 1.530920
        da_energy_sched_tx_mJ      = 1.322059

        da_energy_slot_rx_mJ      = 1.180825
        da_energy_slot_retrans_mJ = 2.537448
        da_energy_slot_tx_mJ      = 2.443399

        da_energy_mJ = da_energy_sched_tx_mJ + nr_nodes * da_energy_slot_tx_mJ + (nr_ids - nr_nodes) * da_energy_slot_rx_mJ


        # Reporting
        # Note: assumption that no commands are sent
        reporting_probability = 1 / nr_nodes
        nr_retransmissions   = 0

        if spreading_factor == 7:
            rp_energy_overhead_mJ = 21.076502
            rp_energy_per_node_mJ = 5.242046
        elif spreading_factor == 10:
            rp_energy_overhead_mJ = 115.437542
            rp_energy_per_node_mJ = 33.22107
        elif spreading_factor == 11:
            rp_energy_overhead_mJ = 254.915094
            rp_energy_per_node_mJ = 66.369789
        else:
            raise ValueError('Unsupported spreading factor %i' % (spreading_factor,))

        rp_energy_slot_mJ = rp_energy_overhead_mJ + nr_nodes * rp_energy_per_node_mJ
        rp_energy_mJ      = (1 + nr_retransmissions) * rp_energy_slot_mJ * reporting_probability

        # Filter if no co-detection
        if nr_nodes < co_detection:
            logging.info('Ignoring reporting costs (%10.3f mJ) for network of %i nodes, as below co-detection threshold of %i' % (rp_energy_mJ, nr_nodes, co_detection,))
            rp_energy_mJ = 0


        # Ack Distribution
        # Note: assumption that no commands are sent
        ad_energy_comm_mJ = 2.893299

        ad_energy_mJ = ad_energy_comm_mJ

        # Filter if single network
        if nr_nodes <= 1:
            ad_energy_mJ = 0

        # Comparison
        tot_energy_mJ = sw_energy_mJ + le_energy_mJ + da_energy_mJ + rp_energy_mJ + ad_energy_mJ

        logging.debug('\nSW: %10.3f mJ (%5.2f %%)\nLE: %10.3f mJ (%5.2f %%)\nDA: %10.3f mJ (%5.2f %%)\nRP: %10.3f mJ (%5.2f %%)\nAD: %10.3f mJ (%5.2f %%)\nEnergy per event: %f mJ' % (
            sw_energy_mJ, (100 * sw_energy_mJ / tot_energy_mJ),
            le_energy_mJ, (100 * le_energy_mJ / tot_energy_mJ),
            da_energy_mJ, (100 * da_energy_mJ / tot_energy_mJ),
            rp_energy_mJ, (100 * rp_energy_mJ / tot_energy_mJ),
            ad_energy_mJ, (100 * ad_energy_mJ / tot_energy_mJ),
            tot_energy_mJ,))

        return [tot_energy_mJ, sw_energy_mJ, le_energy_mJ, da_energy_mJ, rp_energy_mJ, ad_energy_mJ]

    @classmethod
    def simulate_deployment(cls, spreading_factor, battery_size_mAh, deployment_duration_s):

        # HW parameters
        bat_efficiency_percent = 100
        bat_energy_mJ = BAT_VOLTAGE_V * battery_size_mAh * 3600 * (bat_efficiency_percent / 100)

        # Low-power usage
        lpm_energy_mJ = deployment_duration_s * LPM_mW

        # Debugging usage
        health_period_s = 3600
        if spreading_factor == 7:
            # Test 1923
            health_msg_mJ = 27.622493
        elif spreading_factor == 10:
            # Test 1939
            health_msg_mJ = 161.664594
        elif spreading_factor == 11:
            raise ValueError('Spreading factor 11 not supported for simulating deployments')
        else:
            raise ValueError('Unsupported spreading factor %i' % (spreading_factor,))
        health_energy_mJ = (deployment_duration_s / health_period_s) * health_msg_mJ

        logging.info('\nTargeted deployment duration: %i s (%4.1f months) with a health period of %i s (%4.1f min)' % (deployment_duration_s, (deployment_duration_s / (30 * 24 * 3600)), health_period_s, (health_period_s / 60),))
        logging.info('\nEnergy for LPM:    %15.3f mJ (%8.5f %%)\nEnergy for Health: %15.3f mJ (%8.5f %%)' % (lpm_energy_mJ, (100 * lpm_energy_mJ / bat_energy_mJ), health_energy_mJ, (100 * health_energy_mJ / bat_energy_mJ),))

        return [bat_energy_mJ, lpm_energy_mJ, health_energy_mJ]

    @classmethod
    def evaluate_single_network(cls, spreading_factor, battery_size_mAh, nr_nodes=5, deployment_duration_s=1 * 365 * 24 * 3600):

        # Simulate network
        evt_metrics = cls.simulate_event_energy(spreading_factor, nr_nodes=nr_nodes)

        logging.info('\nSW: %10.3f mJ (%5.2f %%)\nLE: %10.3f mJ (%5.2f %%)\nDA: %10.3f mJ (%5.2f %%)\nRP: %10.3f mJ (%5.2f %%)\nAD: %10.3f mJ (%5.2f %%)\nEnergy per event: %f mJ' % (
                evt_metrics[1], (100 * evt_metrics[1] / evt_metrics[0]),
                evt_metrics[2], (100 * evt_metrics[2] / evt_metrics[0]),
                evt_metrics[3], (100 * evt_metrics[3] / evt_metrics[0]),
                evt_metrics[4], (100 * evt_metrics[4] / evt_metrics[0]),
                evt_metrics[5], (100 * evt_metrics[5] / evt_metrics[0]),
                evt_metrics[0],))

        depl_metrics = cls.simulate_deployment(spreading_factor, battery_size_mAh, deployment_duration_s)

        nr_events = (depl_metrics[0] - depl_metrics[1] - depl_metrics[2]) / evt_metrics[0]
        logging.info('Resulting number of events on battery: %i' % (nr_events,))

    @classmethod
    def evaluate_networks(cls, spreading_factor, battery_size_mAh, nr_nodes, nr_ids=32, deployment_duration_s=1 * 365 * 24 * 3600, co_detection=2):

        if not isinstance(nr_nodes, list):
            raise ValueError('Require list of number of nodes to simulate multiple networks')

        # Simulate deployment
        depl_metrics = cls.simulate_deployment(spreading_factor, battery_size_mAh, deployment_duration_s)

        # Simulate different networks
        rows_list  = []
        df_columns = ['Nr Nodes [#]', 'Nr Events [#]', 'Energy Total [mJ]', 'Energy SW [mJ]', 'Energy LE [mJ]', 'Energy DA [mJ]', 'Energy RP [mJ]', 'Energy AD [mJ]']
        str_base   = "\n {0:s} | {1:s} | {2:s} |   {3:s} |   {4:s} |   {5:s} |   {6:s} |   {7:s} |".format(*df_columns)

        for i in nr_nodes:
            evt_metrics = cls.simulate_event_energy(spreading_factor, nr_nodes=i, nr_ids=nr_ids, co_detection=co_detection)
            nr_events   = int((depl_metrics[0] - depl_metrics[1] - depl_metrics[2]) / evt_metrics[0])
            row = {'nr_nodes': i, 'nr_events': nr_events, 'tot': evt_metrics[0], 'sw': evt_metrics[1], 'le': evt_metrics[2], 'da': evt_metrics[3], 'rp': evt_metrics[4], 'ad': evt_metrics[5]}
            rows_list.append(row)

            # Convert for printing
            str_base += "\n           {0:2d} |    {1:10d} |      {2:12.4f} | {3:8.4f} ({4:4.1f}%) | {5:8.4f} ({6:4.1f}%) | {7:8.4f} ({8:4.1f}%) | {9:8.4f} ({10:4.1f}%) | {11:8.4f} ({12:4.1f}%) |".format(
                i, nr_events, evt_metrics[0],
                evt_metrics[1], (100 * evt_metrics[1] / evt_metrics[0]),
                evt_metrics[2], (100 * evt_metrics[2] / evt_metrics[0]),
                evt_metrics[3], (100 * evt_metrics[3] / evt_metrics[0]),
                evt_metrics[4], (100 * evt_metrics[4] / evt_metrics[0]),
                evt_metrics[5], (100 * evt_metrics[5] / evt_metrics[0]),)

        # Print information
        logging.info(str_base)

        # Create dataframe
        stats = pd.DataFrame(rows_list)

        return stats

    @classmethod
    def simulate_static_wakeup(cls, evt_histogram, nr_slots):

        tot_events = sum(evt_histogram)

        # Probability of Tx - floor as Rx is energetically cheaper than Tx and should hence be preferred
        p_tx = floor(nr_slots / 2) / nr_slots

        # Energy
        energy = p_tx * nr_slots * DS_ENERGY_TX_mJ + (1 - p_tx) * nr_slots * DS_ENERGY_RX_mJ

        # Probability of failure
        p_failure_node    = 0
        p_failure_network = 0
        for nr_events in evt_histogram[2:]:
            nr_nodes   = evt_histogram.index(nr_events)

            # P[failure for event with N nodes] = P[failure | Event with N nodes] * P[Event with N nodes] = 1 / nCr(S, n_tx)^(N-1) * n_N / sum(n_N)
            p_failure = 1 / pow(comb(nr_slots, int(p_tx * nr_slots)), nr_nodes - 1)

            p_failure_node    += p_failure                          * (nr_events / tot_events)
            p_failure_network += (1 - pow(1 - p_failure, nr_nodes)) * (nr_events / tot_events)

        return [energy, p_failure_node, p_failure_network, p_tx]

    @classmethod
    def simulate_probabilistic_wakeup(cls, evt_histogram, nr_slots):

        tot_events = sum(evt_histogram)

        # Probability of failure - optimize to find minimal failure probability
        p_rx_opt              = 0
        p_failure_node_min    = 1
        p_failure_network_min = 1
        division_factor       = 10000

        for p_rx in list(range(0, division_factor, 1)):
            p_failure_node    = 0
            p_failure_network = 0
            for nr_events in evt_histogram[2:]:
                nr_nodes = evt_histogram.index(nr_events)

                # P[failure for event with N nodes] = P[never receive for event with N nodes] = P[all other nodes dont send in any Rx slot | event with N nodes] * P[event with N nodes] = p_rx^( (N-1) * S * p_rx) * n_N / sum(n_N)
                p_failure = pow(p_rx / division_factor, (nr_nodes - 1) * nr_slots * p_rx / division_factor)

                p_failure_node    += p_failure                          * (nr_events / tot_events)
                p_failure_network += (1 - pow(1 - p_failure, nr_nodes)) * (nr_events / tot_events)

            if p_failure_node < p_failure_node_min:
                logging.debug('Discovered probability p_rx = %f with a smaller node failure probability %f < %f (network failure probability %f < %f)' % (p_rx / division_factor, p_failure_node, p_failure_node_min, p_failure_network, p_failure_network_min,))

                p_rx_opt              = p_rx / division_factor
                p_failure_node_min    = p_failure_node
                p_failure_network_min = p_failure_network

        # Probability of Tx
        p_tx = 1 - p_rx_opt

        # Energy
        energy = p_tx * nr_slots * DS_ENERGY_TX_mJ + (1 - p_tx) * nr_slots * DS_ENERGY_RX_mJ

        return [energy, p_failure_node_min, p_failure_network_min, p_tx]

    @classmethod
    def evaluate_wakeup_slots(cls, evt_histogram, nr_slots):

        if not isinstance(nr_slots, list):
            raise ValueError('Require list of number of slots to simulate multiple wake-up conditions')

        # Simulate different networks
        rows_list  = []
        df_columns = ['Nr Slots [#]', 'Scheme', 'Energy [mJ]', 'Prob Node Failure [%]', 'Prob Network Failure [%]', 'Prob Tx [%]']
        str_base   = "\n {0:s} | {1:13s} | {2:s} | {3:s} | {4:s} | {5:s} |".format(*df_columns)

        for i in nr_slots:
            static_scheme = cls.simulate_static_wakeup(evt_histogram, i)
            row = {'nr_slots': i, 'scheme': 0, 'energy': static_scheme[0], 'prob_node_failure': static_scheme[1], 'prob_net_failure': static_scheme[2], 'prob_tx': static_scheme[3]}
            rows_list.append(row)

            probab_scheme = cls.simulate_probabilistic_wakeup(evt_histogram, i)
            row = {'nr_slots': i, 'scheme': 1, 'energy': probab_scheme[0], 'prob_node_failure': probab_scheme[1], 'prob_net_failure': probab_scheme[2], 'prob_tx': probab_scheme[3]}
            rows_list.append(row)

            # Convert for printing
            str_base += "\n           {0:2d} | {1:13s} | {2:11.4f} | {3:21.3f} | {4:24.3f} | {5:11.3f} |\n           {6:2d} | {7:13s} | {8:11.4f} | {9:21.3f} | {10:24.3f} | {11:11.3f} |".format(
                i, 'Static',        static_scheme[0], static_scheme[1] * 100, static_scheme[2] * 100, static_scheme[3] * 100,
                i, 'Probabilistic', probab_scheme[0], probab_scheme[1] * 100, probab_scheme[2] * 100, probab_scheme[3] * 100,)

        # Print information
        logging.info(str_base)

        # Create dataframe
        stats = pd.DataFrame(rows_list)

        return stats

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-sf', '--spreading-factor', type=int, dest='sim_sf', default=DEFAULT_SF,
                            help='Spreading factor to be simulated (default: %i).' % (DEFAULT_SF,), metavar='INT')
        parser.add_argument('-b', '--battery', type=int, dest='battery_mAh', default=DEFAULT_BATTERY_SIZE_mAh,
                            help='Battery size to be simulated (default: %imAh).' % (DEFAULT_BATTERY_SIZE_mAh,), metavar='INT')
        parser.add_argument('-n', '--nodes', type=int, dest='nr_nodes', default=DEFAULT_NR_NODES,
                            help='Number of nodes to be simulated (default: %i).' % (DEFAULT_NR_NODES,), metavar='INT')
        parser.add_argument('-d', '--duration', dest='simulate_duration', action='store_true',
                            help='Simulate duration instead of energy.')
        parser.add_argument('-w', '--simulate-wakeup', dest='simulate_wakeup', action='store_true',
                            help='Simulate wakeup probabilities.')
        args = parser.parse_args()

        if args.sim_sf < 5 or args.sim_sf > 12:
            raise ValueError('Invalid spreading factor: %s' % (args.sim_sf,))
        if args.battery_mAh < 0:
            raise ValueError('Invalid battery size: %s' % (args.battery_mAh,))
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    # Pipe root logger to stdout (StreamHandler)
    handler = logging.StreamHandler()
    handler.setLevel(DEFAULT_LOG_LEVEL)
    handler.setFormatter(formatter)
    root.addHandler(handler)

    # Print debug options
    logging.debug('STeC Model CLI options:')
    for arg in vars(args):
        logging.debug(' {}:\t {}'.format(arg, getattr(args, arg) or ''))

    logging.debug("STeC Model script is running...")

    try:

        if args.simulate_wakeup:
            event_histogram = [0, 78768, 9230, 1035, 137, 66, 32, 16, 19, 24, 15, 12, 1, 2, 0, 0, 0, 0, 1, 1]
            nr_sim_slots    = list(range(1, 20))
            EBCSimulator.evaluate_wakeup_slots(event_histogram, nr_sim_slots)
        elif args.simulate_duration:
            # Evaluate event duration
            EBCSimulator.simulate_event_duration(args.sim_sf, args.nr_nodes)
        else:
            # Evaluate one network
            # EBCSimulator.evaluate_single_network(args.sim_sf, args.battery_mAh, args.nr_nodes)

            # Evaluate list of networks
            network_sizes = list(range(1, args.nr_nodes + 1))
            EBCSimulator.evaluate_networks(args.sim_sf, args.battery_mAh, network_sizes)

    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
