#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation of the ALOHA protocol for the STeC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import salabim as sim
from enum import Enum
from math import ceil, floor, log2

from des.des_sensor import GeophoneSensor

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# We simulate with us granularity
_US      = 1
MS_TO_US = 1000
S_TO_MS  = 1000
S_TO_US  = 1000 * 1000

# Global constants
MAX_DELTA_US          = 100 * MS_TO_US
MIN_EVENT_DURATION_US =   2 * S_TO_US
MAX_EVENT_DURATION_US =  15 * S_TO_US

SAMPLE_DURATION_MS = 100

MAX_EVT_PROP_DURATION_MS = MAX_DELTA_US / MS_TO_US

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class ALOHANode(sim.Component):

    class State(Enum):
        INIT      = 0,
        SLEEP     = 1,
        OFFSET    = 2,
        CAD       = 3,
        REPORTING = 4

    def __init__(self, simulator, node_id=0, spreading_factor=7, sample_duration=SAMPLE_DURATION_MS, enable_lpm_costs=True, round_period_s=None, enable_offset=False, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.node_id          = node_id
        self.energy_mJ        = 0
        self.latency_us       = 0
        self.events           = []
        self.nr_evts_total    = 0
        self.nr_evts_reported = 0
        self.sensor           = GeophoneSensor(simulator=simulator, process='', parent_node=self, enable_lpm_costs=enable_lpm_costs)

        self._sim                = simulator
        self._state              = self.State.INIT
        self._rx_queue           = []
        self._is_receiving       = False
        self._last_evt_start     = 0
        self._curr_phase_start   = self._sim.get_time()
        self._curr_phase_end     = 0
        self._curr_phase_energy  = 0
        self._events_reported    = []
        self._spreading_factor   = spreading_factor
        self._sample_duration_ms = sample_duration
        self._enable_lpm_costs   = enable_lpm_costs
        self._enable_offset      = enable_offset

        # Check that no wrong settings have been selected by command line
        if round_period_s is not None:
            logging.warning('Round period should not be set for ALOHA, but is set to %ss' % (round_period_s,))

        # Verify that parameters do not cause corrupted computations later-on
        if self.node_id < self._sim.base_id:
            raise ValueError('Tried to initialize node with node ID %i, but simulation IDs must be higher than %i' % (self.node_id, self._sim.base_id,))

    def process(self):

        # Initialize parameters
        LPM_mW = 0.010485 if self._enable_lpm_costs else 0

        ca_rx_mW           = 37.902833
        ca_ofs_duration_ms = 100

        rp_nr_retransmissions = 0

        if self._spreading_factor == 7:
            health_msg_mJ = 27.622493

            ca_rx_duration_ms = 4.37

            rp_energy_overhead_mJ   = 21.076502
            rp_energy_per_node_mJ   = 5.242046
            rp_duration_overhead_ms = 149.26
            rp_duration_per_node_ms = 18.82
        elif self._spreading_factor == 10:
            health_msg_mJ = 161.664594

            ca_rx_duration_ms = 38.64

            rp_energy_overhead_mJ   = 115.437542
            rp_energy_per_node_mJ   = 33.22107
            rp_duration_overhead_ms = 796.51
            rp_duration_per_node_ms = 111.00
        elif self._spreading_factor == 11:
            health_msg_mJ = None  # TODO: Measure

            ca_rx_duration_ms = 77.77

            rp_energy_overhead_mJ   = 254.915094
            rp_energy_per_node_mJ   = 66.369789
            rp_duration_overhead_ms = 1652.69
            rp_duration_per_node_ms = 220.23
        else:
            raise ValueError('Unsupported spreading factor %i' % (self._spreading_factor,))

        # Start of the state machine
        while True:

            prev_state = self._state  # Store state for debugging
            prev_time  = self._sim.get_time()

            if self._state == self.State.INIT:

                # Wait for a sensor trigger
                self._curr_phase_start = self._sim.get_time()
                self._state            = self.State.SLEEP
                yield self.passivate()

            elif self._state == self.State.SLEEP:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor' % (self.node_id, event.sequence_number(),))

                    # Start communication
                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Update metrics
                    self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

                    # Sampling - stop once the maximal sampling duration has passed
                    sampling_offset_ms = min(event.duration_us / MS_TO_US, self._sample_duration_ms)

                    # Offset - Due to maximal event propagation, as not synchronized
                    rp_duration_slot_ms = rp_duration_overhead_ms + rp_duration_per_node_ms

                    of_duration_ms = (MAX_EVT_PROP_DURATION_MS + rp_duration_slot_ms) * (self.node_id - self._sim.base_id) if self._enable_offset else 0
                    of_energy_mJ   = of_duration_ms / S_TO_MS * LPM_mW

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = of_energy_mJ

                    self._curr_phase_start = self._last_evt_start
                    self._curr_phase_end   = self._sim.get_time() + (sampling_offset_ms + of_duration_ms) * MS_TO_US
                    self._state            = self.State.OFFSET
                    yield self.hold(till=self._curr_phase_end)
                else:
                    logging.warning('Node %i activated without sensor trigger' % (self.node_id,))
                    yield self.passivate()

            elif self._state == self.State.OFFSET:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.OFFSET.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # CAD
                    ca_duration_ms = ca_rx_duration_ms

                    # Action
                    self.start_rx()

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = ca_rx_duration_ms / S_TO_MS * ca_rx_mW

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + ca_duration_ms * MS_TO_US
                    self._state            = self.State.CAD
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.CAD:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.CAD.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                elif len(self._rx_queue) > 0 or self._sim.get_nr_reporters() > 0:
                    # Detected other, re-start CAD
                    if self._sim.get_nr_reporters() > 1:
                        logging.warning('Node %i detected %i simultaneously reporting nodes during CAD' % (self.node_id, self._sim.get_nr_reporters(),))
                    else:
                        logging.debug('Node %i detected a reporting node during CAD, restarting CAD with offset %ims' % (self.node_id, ca_ofs_duration_ms,))

                    self.clear_rx_queue(hard=True)

                    self._curr_phase_energy += ca_ofs_duration_ms / S_TO_MS * LPM_mW + ca_rx_duration_ms / S_TO_MS * ca_rx_mW
                    self._curr_phase_end    += (ca_ofs_duration_ms + ca_rx_duration_ms) * MS_TO_US
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Report events
                    self.update_reported_evts()

                    # Reporting
                    curr_nr_events = len(self._events_reported)

                    rp_duration_slot_ms = rp_duration_overhead_ms + curr_nr_events * rp_duration_per_node_ms
                    rp_duration_ms      = (1 + rp_nr_retransmissions) * rp_duration_slot_ms - ca_rx_duration_ms  # Subtract CAD once as this is included in the re-transmission

                    rp_energy_slot_mJ = rp_energy_overhead_mJ + curr_nr_events * rp_energy_per_node_mJ
                    rp_energy_mJ      = (1 + rp_nr_retransmissions) * rp_energy_slot_mJ - ca_rx_duration_ms / S_TO_MS * ca_rx_mW  # Subtract CAD once as this is included in the re-transmission

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = rp_energy_mJ

                    # Stop Rx and broadcast packet
                    self.stop_rx()
                    self._sim.broadcast_packet(ALOHAPacket(sender=self), add_tx_to_own_queue=False)

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + rp_duration_ms * MS_TO_US
                    self._state            = self.State.REPORTING
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.REPORTING:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.REPORTING.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Update reporting statistics
                    for event in self._events_reported:
                        self.latency_us       += self._sim.get_time() - event.get_start_time()
                        self.nr_evts_reported += 1

                    # Remove all events associated with this node
                    self.clear_reported_evts(reported=True)

                    # Prepare state for next round
                    self._curr_phase_energy = 0

                    if len(self.events) > 0:

                        # Sampling - stop once the maximal sampling duration of the next event has passed
                        remaining_sampling_us = max(0, self.events[0].duration_us - self.events[0].get_active_time())
                        sampling_offset_ms    = min(remaining_sampling_us / MS_TO_US, self._sample_duration_ms)

                        # Offset - Due to maximal event propagation, as not synchronized
                        of_duration_ms = MAX_EVT_PROP_DURATION_MS * (self.node_id - self._sim.base_id) if self._enable_offset else 0
                        of_energy_mJ   = of_duration_ms / S_TO_MS * LPM_mW
                        logging.debug('Node %i observed %i events during communication; scheduling additional RP in %ims' % (self.node_id, len(self.events), of_duration_ms,))

                        # Store prospective energy costs during this phase
                        self._curr_phase_energy = of_energy_mJ

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._sim.get_time() + (sampling_offset_ms + of_duration_ms) * MS_TO_US
                        self._state            = self.State.OFFSET
                        yield self.hold(till=self._curr_phase_end)
                    else:
                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = 0
                        self._state            = self.State.SLEEP
                        yield self.passivate()

            logging.debug('Node %i switched from state %9s to state %9s at time %ius' % (self.node_id, prev_state.name, self._state.name, prev_time,))

    def terminate(self):

        # Stop sensor
        self.sensor.terminate()

        # Include LPM costs since last phase
        if self._enable_lpm_costs:
            LPM_mW = 0.010485

            self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

        # Stop itself
        self.cancel()

    def is_reporting(self):
        return self._state == self.State.REPORTING

    def start_rx(self):
        if self._is_receiving:
            logging.warning('Node %i is already receiving' % (self.node_id,))
        elif len(self._rx_queue) > 0:
            logging.warning('Node %i tried to start receiving, but Rx queue already contains %i elements' % (self.node_id, len(self._rx_queue),))
        else:
            self._is_receiving = True

    def stop_rx(self):
        if not self._is_receiving:
            logging.warning('Node %i is expected to be receiving, but is not' % (self.node_id,))
        else:
            self._is_receiving = False

        if len(self._rx_queue) > 0:
            logging.debug('Node %i still had packets in its Rx queue when stopping Rx' % (self.node_id,))
            self.clear_rx_queue(hard=True)

    def rx(self, packet, activate=False):
        if self._is_receiving:
            self._rx_queue.append(packet)

            if activate:
                logging.warning('Node %i activated following a message of type %s in state %s at time %ius' % (self.node_id, packet.type.name, self._state.name, self._sim.get_time(),))
                self.activate()

    def clear_rx_queue(self, hard=False):
        if hard:
            # Completely empty Rx queue, including packets received in the current simulation step
            self._rx_queue.clear()
        else:
            # Remove all old packets, but keep the ones received in the current simulation step
            new_packets = []

            for packet in self._rx_queue:
                if packet.timestamp >= self._sim.get_time():
                    new_packets.append(packet)

            # Overwrite Rx queue
            self._rx_queue = new_packets

    def update_reported_evts(self, report_all=False):
        if len(self._events_reported) > 0:
            logging.warning('Expected reported event list to be empty, but contained %i elements' % (len(self._events_reported),))
            return

        remaining_evts = []
        for event in self.events:
            # If event has been sampled long enough, we can send it off
            if event.get_active_time() > self._sample_duration_ms or report_all:
                self._events_reported.append(event)
            else:
                remaining_evts.append(event)

        if len(self.events) != len(self._events_reported) + len(remaining_evts):
            logging.error('Expected to split events into two separate queues, but corrupted data')
        else:
            self.events = remaining_evts
            logging.debug('Node %i added %i events for reporting, %i events remain sampling at time %ius' % (self.node_id, len(self._events_reported), len(self.events), self._sim.get_time(),))

    def clear_reported_evts(self, reported):
        if len(self._events_reported) == 0:
            return
        elif reported:
            logging.debug('Node %i removed %i reported events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))
        else:
            logging.debug('Node %i removed %i suppressed events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))

        [event.terminate(reported) for event in self._events_reported]
        self._events_reported.clear()


class ALOHAPacket:

    def __init__(self, sender):
        self.sender = sender

        self.timestamp = self.sender._sim.get_time()
