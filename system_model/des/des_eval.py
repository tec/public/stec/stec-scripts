#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation evaluation script for the EBC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import os
import sys
import pandas as pd
from argparse import ArgumentParser

sys.path.append("../")
sys.path.append("../../")
sys.path.append("../../../analysis")
from des.des_simulator import Simulator
from des.des_aloha     import ALOHANode
from des.des_ebc       import EBCNode
from des.des_elwb      import ELWBNode

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

DEFAULT_LOG_LEVEL = 'INFO'

LOG_FORMAT      = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FORMAT = '%H:%M:%S'

FILE_DIR             = os.path.dirname(__file__)
DEFAULT_METRICS_FILE = '/metrics_'

# Simulation parameters
MAX_NR_EVENTS       = 100 * 1000
MAX_NR_EVENTS_TRACE = 50

DEFAULT_SPREADING_FACTOR   = 7
DEFAULT_CODET_THRESHOLD    = 2
DEFAULT_PERIOD_S           = 15
DEFAULT_SAMPLE_DURATION_MS = 100

SPREADING_FACTORS   = [7, 10, 11]
CODET_THRESHOLDS    = range(2, 20)
ELWB_PERIODS_S      = [5, 10, 15, 30, 60, 120, 300]
SAMPLE_DURATIONS_MS = [100, 250, 500, 1000, 2000, 5000, 10000, 15000]
EVT_INTERVAL_S      = [5, 15, 30, 60, 300, 900, 1800, 3600, 7200, 21600, 86400]

# ----------------------------------------------------------------------------------------------------------------------
# Functions
# ----------------------------------------------------------------------------------------------------------------------


def load_metrics(load_ebc=False, load_elwb=False, load_aloha=False, postfix='', relative_path='', permit_reduced_events=False):
    if load_ebc:
        file_ending = 'ebc'   + postfix + '.pkl'
    elif load_elwb:
        file_ending = 'elwb'  + postfix + '.pkl'
    elif load_aloha:
        file_ending = 'aloha' + postfix + '.pkl'
    else:
        raise ValueError('Unknown network type')

    file_path = FILE_DIR + relative_path + DEFAULT_METRICS_FILE + file_ending

    # Verify that file does exist
    if not os.path.isfile(file_path):

        # Check whether we can also use a test with less than the required number of co-detections
        if not permit_reduced_events:
            logging.warning('Invalid path to load metrics: %s' % (file_path,))
            return None
        else:
            dir_name   = FILE_DIR + relative_path
            file_names = [f for f in os.listdir(dir_name) if os.path.isfile(os.path.join(dir_name, f))]

            # Check whether there exists a metrics file for the given test
            test_id = int(postfix[-4:]) if len(postfix) >= 4 else 0
            for file_name in file_names:
                if 'test_%i' % (test_id,) in file_name:
                    file_path = FILE_DIR + relative_path + file_name

            if not os.path.isfile(file_path):
                logging.warning('Invalid path to load metrics: %s' % (file_path,))
                return None
            else:
                logging.info('Replaced original path to metrics (%s) with new one (%s)' % (FILE_DIR + relative_path + DEFAULT_METRICS_FILE + file_ending, file_path,))

    # Load dataframe
    df = pd.read_pickle(file_path)
    logging.info('Loaded metrics from path %s' % (file_path,))

    return df


def generate_postfix(sf, cd, sd, p=None):
    postfix = '_final_sf_%i_cd_%i_sd_%i' % (sf, cd, sd,)

    if p is not None:
        postfix += '_p_%i' % (p,)

    return postfix

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-a', '--aloha', dest='simulate_aloha', action='store_true',
                            help='Simulate the ALOHA protocol.')
        parser.add_argument('-b', '--elwb',  dest='simulate_elwb', action='store_true',
                            help='Simulate the eLWB protocol.')
        parser.add_argument('-c', '--ebc', dest='simulate_ebc', action='store_true',
                            help='Simulate the STeC protocol.')
        parser.add_argument('-s', '--store', dest='store_metrics', action='store_true',
                            help='Store the resulting metrics as a file.')
        parser.add_argument('-f', '--store-final', dest='store_final_metrics', action='store_true',
                            help='Store the resulting final metrics as a file.')
        parser.add_argument('-r', '--real-data', dest='use_real_data', action='store_true',
                            help='Use real trace data from the deployment as input.')
        parser.add_argument('-sf', '--spreading-factor', type=int, dest='spreading_factor', default=DEFAULT_SPREADING_FACTOR,
                            help='Spreading factor (default: %d).' % (DEFAULT_SPREADING_FACTOR,), metavar='INT')
        parser.add_argument('-cd', '--co-detection', type=int, dest='codet_threshold', default=DEFAULT_CODET_THRESHOLD,
                            help='Co-detection threshold (default: %d).' % (DEFAULT_CODET_THRESHOLD,), metavar='INT')
        parser.add_argument('-p', '--period', type=int, dest='period_s', default=DEFAULT_PERIOD_S,
                            help='Round period (default: %ds).' % (DEFAULT_PERIOD_S,), metavar='INT')
        parser.add_argument('-sd', '--sample-duration', type=int, dest='sample_duration_ms', default=DEFAULT_SAMPLE_DURATION_MS,
                            help='Sampling duration (default: %dms).' % (DEFAULT_SAMPLE_DURATION_MS,), metavar='INT')
        parser.add_argument('-e', '--euler', dest='use_euler', action='store_true',
                            help='Run script on Euler.')
        parser.add_argument('-t', '--trace', dest='use_trace', action='store_true',
                            help='Use a trace file to simulate events.')
        parser.add_argument('-tp', '--trace-path', type=str, dest='trace_path',
                            help='Path to .log file of trace.', metavar='STRING')
        cli_args = parser.parse_args()

    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    # Pipe root logger to stdout (StreamHandler)
    handler = logging.StreamHandler()
    handler.setLevel(DEFAULT_LOG_LEVEL)
    handler.setFormatter(formatter)
    root.addHandler(handler)

    # Print debug options
    logging.debug('DES CLI options:')
    for arg in vars(cli_args):
        logging.debug(' {}:\t {}'.format(arg, getattr(cli_args, arg) or ''))

    logging.debug('Discrete Event Simulation evaluation script is running...')

    try:

        # Decide whether we run the script directly or recursively for parallelization
        if cli_args.use_euler:

            for spreading_factor in SPREADING_FACTORS:
                for codet_threshold in CODET_THRESHOLDS:

                    if cli_args.simulate_ebc:
                        command = 'bsub -o ./output/log_ebc.txt -R \"rusage[mem=%i]\" \"python ./des_eval.py -c -r -f -sf %i -cd %i -sd %i\"'                 % (   2048, spreading_factor, codet_threshold, DEFAULT_SAMPLE_DURATION_MS,)
                    elif cli_args.simulate_elwb:
                        command = 'bsub -o ./output/log_elwb.txt -W %i:00 -R \"rusage[mem=%i]\" \"python ./des_eval.py -b -r -f -sf %i -cd %i -sd %i -p %i\"' % (4, 8192, spreading_factor, codet_threshold, DEFAULT_SAMPLE_DURATION_MS, DEFAULT_PERIOD_S,)
                    elif cli_args.simulate_aloha:
                        command = 'bsub -o ./output/log_aloha.txt -R \"rusage[mem=%i]\" \"python ./des_eval.py -a -r -f -sf %i -cd %i -sd %i\"'               % (   2048, spreading_factor, codet_threshold, DEFAULT_SAMPLE_DURATION_MS,)
                    else:
                        raise ValueError('Must specify node type (no valid CLI option found)')

                    # Execute command
                    os.system(command)
        elif cli_args.use_trace:

            # Verify that trace path is given
            if cli_args.trace_path is None:
                raise ValueError('Must specify trace path (no valid CLI argument found)')

            # Generate simulator
            des_sim = Simulator()
            des_sim.init_simulation(max_nr_events=MAX_NR_EVENTS_TRACE, codet_threshold=cli_args.codet_threshold, use_trace_data=cli_args.use_trace, trace_path=cli_args.trace_path, log_metrics=cli_args.store_metrics, store_metrics=cli_args.store_metrics)

            if cli_args.simulate_ebc:
                des_sim.generate_network(EBCNode,  spreading_factor=cli_args.spreading_factor, sample_duration=cli_args.sample_duration_ms)
            elif cli_args.simulate_elwb:
                des_sim.generate_network(ELWBNode, spreading_factor=cli_args.spreading_factor, sample_duration=cli_args.sample_duration_ms, period=cli_args.period_s)
            else:
                raise ValueError('Must specify node type (no valid CLI option found)')

            # Run environment
            des_sim.start_simulation(duration_us=None)

            # Store statistics
            if cli_args.store_final_metrics:

                # Generate postfix
                curr_postfix = generate_postfix(cli_args.spreading_factor, cli_args.codet_threshold, cli_args.sample_duration_ms, cli_args.period_s if cli_args.simulate_elwb else None)

                # Fetch and store metrics
                final_metrics = des_sim.get_node_statistics(return_as_df=True)

                # Also store some overall metrics (is stored for each node to maintain the data structure)
                evt_metrics = des_sim.get_evt_statistics()
                for evt_metric in evt_metrics.keys():
                    final_metrics[evt_metric] = evt_metrics[evt_metric]

                trace_postfix = ('c_%i' % MAX_NR_EVENTS_TRACE) + cli_args.trace_path[len('c_1000'):]
                des_sim.store_metrics(final_metrics, postfix=curr_postfix + '_' + trace_postfix)

            if cli_args.simulate_ebc:
                node_type = EBCNode
            elif cli_args.simulate_elwb:
                node_type = ELWBNode
            else:
                node_type = None
            logging.info('Finished trace simulation of node type %s with SF %i, Co-detection threshold %i, Sample duration %ims, Period %is with trace %s' % (node_type.__name__ if node_type is not None else '[invalid]', cli_args.spreading_factor, cli_args.codet_threshold, cli_args.sample_duration_ms, cli_args.period_s, cli_args.trace_path))

        else:

            # Generate simulator
            des_sim = Simulator()
            des_sim.init_simulation(max_nr_events=MAX_NR_EVENTS, codet_threshold=cli_args.codet_threshold, use_trace_data=cli_args.use_real_data, use_real_data=cli_args.use_real_data, log_metrics=cli_args.store_metrics, store_metrics=cli_args.store_metrics)

            if cli_args.simulate_ebc:
                des_sim.generate_network(EBCNode,   spreading_factor=cli_args.spreading_factor, sample_duration=cli_args.sample_duration_ms)
            elif cli_args.simulate_elwb:
                des_sim.generate_network(ELWBNode,  spreading_factor=cli_args.spreading_factor, sample_duration=cli_args.sample_duration_ms, period=cli_args.period_s)
            elif cli_args.simulate_aloha:
                des_sim.generate_network(ALOHANode, spreading_factor=cli_args.spreading_factor, sample_duration=cli_args.sample_duration_ms)
            else:
                raise ValueError('Must specify node type (no valid CLI option found)')

            # Run environment
            des_sim.start_simulation(duration_us=None)

            # Store statistics
            if cli_args.store_final_metrics:

                # Generate postfix
                curr_postfix = generate_postfix(cli_args.spreading_factor, cli_args.codet_threshold, cli_args.sample_duration_ms, cli_args.period_s if cli_args.simulate_elwb else None)

                # Fetch and store metrics
                final_metrics = des_sim.get_node_statistics(return_as_df=True)

                # Also store some overall metrics (is stored for each node to maintain the data structure)
                evt_metrics = des_sim.get_evt_statistics()
                for evt_metric in evt_metrics.keys():
                    final_metrics[evt_metric] = evt_metrics[evt_metric]

                des_sim.store_metrics(final_metrics, postfix=curr_postfix)

            if cli_args.simulate_ebc:
                node_type = EBCNode
            elif cli_args.simulate_elwb:
                node_type = ELWBNode
            elif cli_args.simulate_aloha:
                node_type = ALOHANode
            else:
                node_type = None
            logging.info('Finished simulation of node type %s with SF %i, Co-detection threshold %i, Sample duration %ims, Period %is' % (node_type.__name__ if node_type is not None else '[invalid]', cli_args.spreading_factor, cli_args.codet_threshold, cli_args.sample_duration_ms, cli_args.period_s,))

    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
