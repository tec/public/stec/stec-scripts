#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation of the eLWB protocol for the STeC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import salabim as sim
from enum     import Enum
from math     import ceil, floor, log2
from random   import choice, seed, randint
from operator import itemgetter

from des.des_sensor import GeophoneSensor

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# We simulate with us granularity
_US      = 1
MS_TO_US = 1000
S_TO_MS  = 1000
S_TO_US  = 1000 * 1000

# Global constants
MAX_DELTA_US          = 100 * MS_TO_US
MIN_EVENT_DURATION_US =   2 * S_TO_US
MAX_EVENT_DURATION_US =  15 * S_TO_US

SAMPLE_DURATION_MS = 100

ROUND_PERIOD_S = 15

# Simulation artefacts
ELWB_SCHED_EARLY_WAKEUP_US = 2

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class ELWBNode(sim.Component):

    class State(Enum):
        INIT              = 0,
        WAIT_FOR_SCHEDULE = 1,
        EVENT_PENDING     = 2,
        SCHEDULE          = 3,
        CONTENTION        = 4,
        EVENT_SLOTS       = 5,
        DATA_AGGREGATION  = 6,
        REPORTING         = 7

    def __init__(self, simulator, node_id=0, spreading_factor=7, sample_duration=SAMPLE_DURATION_MS, enable_lpm_costs=True, round_period_s=ROUND_PERIOD_S, simulate_fsk=True, report_codets_only=True, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.node_id          = node_id
        self.energy_mJ        = 0
        self.latency_us       = 0
        self.events           = []
        self.nr_evts_total    = 0
        self.nr_evts_reported = 0
        self.sensor           = GeophoneSensor(simulator=simulator, process='', parent_node=self, enable_lpm_costs=enable_lpm_costs)

        self._sim                = simulator
        self._state              = self.State.INIT
        self._rx_queue           = []
        self._is_receiving       = False
        self._last_evt_start     = 0
        self._curr_phase_start   = self._sim.get_time()
        self._curr_phase_end     = 0
        self._curr_phase_energy  = 0
        self._next_sched_start   = 0
        self._events_reported    = []
        self._spreading_factor   = spreading_factor
        self._sample_duration_ms = sample_duration
        self._simulate_fsk       = simulate_fsk
        self._is_leader          = False
        self._round_period_s     = round_period_s if round_period_s is not None else ROUND_PERIOD_S
        self._enable_lpm_costs   = enable_lpm_costs
        self._report_codets_only = report_codets_only

    def process(self):

        # Initialize parameters
        LPM_mW = 0.010485 if self._enable_lpm_costs else 0

        da_duration_startup_ms          = 49.79
        da_duration_transition_event_ms = 11.97
        da_duration_transition_data_ms  = 21.61

        rp_nr_retransmissions     = 0
        rp_duration_transition_ms = 23.78

        # Data Aggregation costs
        if self._simulate_fsk:
            da_sched_slot_retrans_mJ = 1.588438
            da_sched_slot_tx_mJ      = 1.494479
            da_req_slot_rx_mJ        = 1.053686
            da_req_slot_retrans_mJ   = 1.028621
            da_req_slot_tx_mJ        = 0.909845
            da_data_slot_retrans_mJ  = 2.793738
            da_data_slot_tx_mJ       = 2.480154

            da_duration_sched_slot_ms = 25.48
            da_duration_req_slot_ms   = 22.19
            da_duration_data_slot_ms  = 30.30
        elif self._spreading_factor == 7:
            da_sched_slot_retrans_mJ = 41.931794
            da_sched_slot_tx_mJ      = 41.820961
            da_req_slot_rx_mJ        = 11.094955
            da_req_slot_retrans_mJ   = 28.204956
            da_req_slot_tx_mJ        = 25.377381
            da_data_slot_retrans_mJ  = 109.364110
            da_data_slot_tx_mJ       = 98.835069

            da_duration_sched_slot_ms = 588.86
            da_duration_req_slot_ms   = 217.58
            da_duration_data_slot_ms  = 581.83
        elif self._spreading_factor == 10:
            da_sched_slot_retrans_mJ = 268.040861
            da_sched_slot_tx_mJ      = 267.185125
            da_req_slot_rx_mJ        = 75.064442
            da_req_slot_retrans_mJ   = 176.621584
            da_req_slot_tx_mJ        = 174.877444
            da_data_slot_retrans_mJ  = 640.502861
            da_data_slot_tx_mJ       = 578.859054

            da_duration_sched_slot_ms = 3577.43
            da_duration_req_slot_ms   = 1448.33
            da_duration_data_slot_ms  = 3577.68
        elif self._spreading_factor == 11:
            raise ValueError('eLWB simulation does not currently support flooding on SF11')
        else:
            raise ValueError('Unsupported spreading factor %i' % (self._spreading_factor,))

        # Reporting and Health costs
        if self._spreading_factor == 7:
            health_msg_mJ = 27.622493

            rp_energy_overhead_mJ   = 21.076502
            rp_energy_per_node_mJ   = 5.242046
            rp_duration_overhead_ms = 149.26
            rp_duration_per_node_ms = 18.82
        elif self._spreading_factor == 10:
            health_msg_mJ = 161.664594

            rp_energy_overhead_mJ   = 115.437542
            rp_energy_per_node_mJ   = 33.22107
            rp_duration_overhead_ms = 796.51
            rp_duration_per_node_ms = 111.00
        elif self._spreading_factor == 11:
            health_msg_mJ = None  # TODO: Measure

            rp_energy_overhead_mJ   = 254.915094
            rp_energy_per_node_mJ   = 66.369789
            rp_duration_overhead_ms = 1652.69
            rp_duration_per_node_ms = 220.23
        else:
            raise ValueError('Unsupported spreading factor %i' % (self._spreading_factor,))

        # Start of the state machine
        while True:

            prev_state = self._state  # Store state for debugging
            prev_time  = self._sim.get_time()

            if self._state == self.State.INIT:

                # Wait for a sensor trigger or a schedule
                self._next_sched_start = self._sim.get_time() + self._round_period_s * S_TO_US

                self._curr_phase_start = self._sim.get_time()
                self._curr_phase_end   = self._next_sched_start - ELWB_SCHED_EARLY_WAKEUP_US
                self._state            = self.State.WAIT_FOR_SCHEDULE
                yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.WAIT_FOR_SCHEDULE or self._state == self.State.EVENT_PENDING:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self._state.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Update metrics - done depending on state below
                    if self._state != self.State.EVENT_PENDING:
                        # Add energy spent in WAIT_FOR_SCHEDULE
                        self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

                        # Overwrite statistics if new in this phase
                        self._curr_phase_start = self._sim.get_time()
                        self._state            = self.State.EVENT_PENDING
                    yield self.hold(till=self._curr_phase_end)
                elif self._sim.get_time() >= self._curr_phase_end:

                    # Action
                    self.start_rx()  # Start listening for packets

                    # NOTE: self._curr_phase_start is NOT updated, as the costs for WAIT_FOR_SCHEDULE / EVENT_PENDING and SCHEDULE are combined
                    self._curr_phase_end = self._next_sched_start
                    self._state          = self.State.SCHEDULE

                    if self._is_leader:
                        # If leader, broadcast schedule
                        yield self.hold(duration=ELWB_SCHED_EARLY_WAKEUP_US / 2)

                        # Make sure nothing triggers node too early
                        while self._sim.get_time() < (self._curr_phase_end - ELWB_SCHED_EARLY_WAKEUP_US / 2):
                            logging.warning('Node %i woke up too early for schedule distribution planned at %ius at time %ius' % (self.node_id, (self._curr_phase_end - ELWB_SCHED_EARLY_WAKEUP_US / 2), self._sim.get_time(),))
                            yield self.hold(till=self._curr_phase_end - ELWB_SCHED_EARLY_WAKEUP_US / 2)

                        curr_reporter = choice(self._sim.get_node_ids())
                        self._sim.broadcast_packet(ELWBPacket(sender=self, packet_type=ELWBPacket.Type.SCHEDULE, leader=self.node_id, reporter=curr_reporter))
                        logging.debug('Leader %i elected node %i as reporter for current round at time %ius' % (self.node_id, curr_reporter, self._sim.get_time(),))

                        yield self.hold(till=self._curr_phase_end)
                    else:
                        # Prepare for receiving schedule
                        yield self.hold(till=self._curr_phase_end)
                else:
                    logging.warning('Node %i activated while waiting for schedule' % (self.node_id,))
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.SCHEDULE:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.SCHEDULE.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                elif self.received_schedule():

                    schedule = self._rx_queue[0]  # Schedule is first (and only) element in queue
                    if not (schedule.sender.is_leader() and schedule.sender.node_id == schedule.leader and schedule.type == ELWBPacket.Type.SCHEDULE):
                        logging.warning('Node %i expected schedule but received packet of type %s from node %i at time %ius' % (self.node_id, schedule.type, schedule.sender.node_id, self._sim.get_time(),))
                    elif self._next_sched_start != self._sim.get_time():
                        logging.warning('Node %i erroneously expected to wake up for next schedule (scheduled for distribution at %ius) at time %ius' % (self.node_id, self._next_sched_start, self._sim.get_time(),))

                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += elapsed_time_us / S_TO_US * LPM_mW

                    # Data Aggregation (eLWB round)
                    self.update_reported_evts()

                    da_duration_ms  = da_duration_startup_ms
                    da_duration_ms += da_duration_sched_slot_ms + da_duration_req_slot_ms  # Event Contention slot schedules
                    da_energy_mJ    = da_sched_slot_retrans_mJ  + da_req_slot_retrans_mJ

                    # Action
                    if len(self._events_reported) > 0:
                        self._sim.broadcast_packet(ELWBPacket(sender=self, packet_type=ELWBPacket.Type.REQUEST, nr_events=len(self._events_reported)))

                    self._next_sched_start = self._sim.get_time() + self._round_period_s * S_TO_US  # Prepare start of next round

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = da_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + da_duration_ms * MS_TO_US
                    self._state            = self.State.CONTENTION
                    yield self.hold(till=self._curr_phase_end)
                else:
                    logging.error('Node %i expected schedule at time %i but did not receive anything with Rx %s' % (self.node_id, self._sim.get_time(), 'enabled' if self._is_receiving else 'disabled',))
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.CONTENTION:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.CONTENTION.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Data Aggregation - Contention
                    contention_observed = self.get_nr_elwb_requests() > 0
                    if contention_observed:
                        da_duration_ms  = da_duration_req_slot_ms   # Used contention slot
                        da_duration_ms += da_duration_transition_event_ms

                        da_energy_mJ = da_req_slot_retrans_mJ
                    else:
                        da_duration_ms = da_duration_req_slot_ms    # Unused contention slot
                        da_energy_mJ   = da_req_slot_rx_mJ

                    # Action
                    if len(self._events_reported) > 0:
                        reported_timings = self.get_local_event_timings()
                        self._sim.broadcast_packet(ELWBPacket(sender=self, packet_type=ELWBPacket.Type.DATA, nr_events=len(self._events_reported), evt_timings=reported_timings))

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = da_energy_mJ

                    if contention_observed:
                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._sim.get_time() + da_duration_ms * MS_TO_US
                        self._state = self.State.EVENT_SLOTS
                    else:
                        # Make sure that the state is not corrupted
                        if len(self._events_reported) > 0:
                            logging.error('Node %i did not observe contention despite having %i events to report at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._sim.get_time() + da_duration_ms * MS_TO_US
                        self._state = self.State.REPORTING
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.EVENT_SLOTS:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.EVENT_SLOTS.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Data Aggregation - Events and Data
                    nr_requests = self.get_nr_elwb_requests()
                    nr_events   = self.get_nr_elwb_events()
                    if nr_requests > 0:
                        da_duration_ms  = da_duration_sched_slot_ms  # Event slots
                        da_duration_ms += self._sim.nr_ids * da_duration_req_slot_ms
                        da_duration_ms += da_duration_transition_data_ms
                        da_duration_ms += da_duration_sched_slot_ms  # Data slots
                        da_duration_ms += nr_events * da_duration_data_slot_ms
                        da_duration_ms += da_duration_req_slot_ms    # Data ACK slot

                        da_energy_mJ  = da_sched_slot_retrans_mJ     # Event slots
                        da_energy_mJ += (self._sim.nr_ids - nr_requests) * da_req_slot_rx_mJ + nr_requests * da_req_slot_retrans_mJ
                        da_energy_mJ += da_sched_slot_retrans_mJ     # Data slots
                        da_energy_mJ += nr_events * da_data_slot_retrans_mJ
                        da_energy_mJ += da_req_slot_retrans_mJ       # Data ACK slot
                    else:
                        logging.error('Node %i observed contention but did not receive packets at time %i' % (self.node_id, self._sim.get_time(),))
                        da_duration_ms = 0
                        da_energy_mJ   = 0

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = da_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + da_duration_ms * MS_TO_US
                    self._state            = self.State.DATA_AGGREGATION
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.DATA_AGGREGATION:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.DATA_AGGREGATION.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Reporting
                    nr_requests = self.get_nr_elwb_requests()
                    nr_events   = self.get_nr_elwb_events()
                    evt_timings = self.get_elwb_event_timings()

                    nr_codet_events = self.observed_codet_events(evt_timings)

                    rp_duration_slot_ms = rp_duration_overhead_ms + nr_codet_events * rp_duration_per_node_ms
                    rp_duration_ms      = (1 + rp_nr_retransmissions) * rp_duration_slot_ms + rp_duration_transition_ms

                    rp_energy_slot_mJ   = rp_energy_overhead_mJ + nr_codet_events * rp_energy_per_node_mJ
                    rp_energy_mJ        = (1 + rp_nr_retransmissions) * rp_energy_slot_mJ

                    # Only count reporting costs if applicable
                    if not self._simulate_fsk:
                        rp_duration_ms = 0  # If we dont use FSK, the base can directly receive the DA phase and does not require another report
                        rp_energy_mJ   = 0
                    elif nr_events == 0:
                        logging.warning('Node %i is ignoring reporting for empty schedule at time %ius' % (self.node_id, self._sim.get_time(),))
                        rp_duration_ms = 0  # Filter if empty schedule
                        rp_energy_mJ   = 0
                    elif nr_codet_events == 0:
                        logging.debug('Node %i is ignoring reporting for network of %i nodes, as no co-detection detected in %i events' % (self.node_id, nr_requests, nr_events,))
                        rp_duration_ms = 0  # Filter if no co-detection
                        rp_energy_mJ   = 0

                        # Remove reported events to avoid adding them to the statistics
                        self.clear_reported_evts(reported=False)
                    elif not self.is_curr_reporter():
                        # NOTE: Because the latency computation is based on the duration of the reporting, we count the reporting time for all nodes (including nodes which do not report), but do not include the costs
                        # rp_duration_ms = 0

                        # Do not pay reporter costs if this node is not assigned to report
                        rp_energy_mJ = 0
                    else:
                        logging.debug('Node %i is reporting co-detection of up to %i nodes with a total of %i co-detected events' % (self.node_id, nr_requests, nr_codet_events,))

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = rp_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + rp_duration_ms * MS_TO_US
                    self._state            = self.State.REPORTING
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.REPORTING:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.REPORTING.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Update reporting statistics
                    for event in self._events_reported:
                        self.latency_us       += self._sim.get_time() - event.get_start_time()
                        self.nr_evts_reported += 1

                    # Remove all events associated with this node
                    self.clear_reported_evts(reported=True)

                    # Prepare state for next round
                    self._curr_phase_energy = 0
                    self.clear_rx_queue(hard=True)

                    # End of round - reset schedule state
                    self.stop_rx()

                    if self._next_sched_start < self._sim.get_time():
                        logging.error('Node %i is expecting its next schedule in the past (scheduled at %ius, which was %ius ago)' % (self.node_id, self._next_sched_start, self._sim.get_time() - self._next_sched_start,))

                    if len(self.events) > 0:
                        logging.debug('Node %i observed %i events during communication' % (self.node_id, len(self.events),))

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._next_sched_start - ELWB_SCHED_EARLY_WAKEUP_US
                        self._state            = self.State.EVENT_PENDING
                    else:
                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._next_sched_start - ELWB_SCHED_EARLY_WAKEUP_US
                        self._state            = self.State.WAIT_FOR_SCHEDULE
                    yield self.hold(till=self._curr_phase_end)

            logging.debug('Node %i switched from state %17s to state %17s at time %ius' % (self.node_id, prev_state.name, self._state.name, prev_time,))

    def terminate(self):

        # Stop sensor
        self.sensor.terminate()

        # Include LPM costs since last phase
        if self._enable_lpm_costs:
            LPM_mW = 0.010485

            self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

        # Stop itself
        self.cancel()

    def is_active(self):
        return len(self.events) > 0 or len(self._events_reported) > 0

    def is_reporting(self):
        return self._state == self.State.REPORTING

    def is_curr_reporter(self):
        curr_reporter = []

        for packet in self._rx_queue:
            if packet.type == ELWBPacket.Type.SCHEDULE and packet.leader is not None and packet.reporter is not None:
                # logging.debug('Node %i observes that leader %i assigned node %i as reporter' % (self.node_id, packet.leader, packet.reporter,))
                curr_reporter.append(packet.reporter)

        if   len(curr_reporter) == 1:
            return curr_reporter[0] == self.node_id
        elif len(curr_reporter) == 0:
            logging.error('Node %i did not observe any assigned reporters for its network' % (self.node_id,))
            return False
        else:
            logging.error('Node %i observed multiple assigned reporters for its network (%s)' % (self.node_id, curr_reporter,))
            return False

    def start_rx(self):
        if self._is_receiving:
            logging.warning('Node %i is already receiving' % (self.node_id,))
        elif len(self._rx_queue) > 0:
            logging.warning('Node %i tried to start receiving, but Rx queue already contains %i elements' % (self.node_id, len(self._rx_queue),))
        else:
            self._is_receiving = True

    def stop_rx(self):
        if not self._is_receiving:
            logging.warning('Node %i is expected to be receiving, but is not' % (self.node_id,))
        else:
            self._is_receiving = False

        if len(self._rx_queue) > 0:
            logging.debug('Node %i still had packets in its Rx queue when stopping Rx' % (self.node_id,))
            self.clear_rx_queue(hard=True)

    def rx(self, packet, activate=False):
        if self._is_receiving:
            self._rx_queue.append(packet)

            if activate:
                logging.warning('Node %i activated following a message of type %s in state %s at time %ius' % (self.node_id, packet.type.name, self._state.name, self._sim.get_time(),))
                self.activate()

    def clear_rx_queue(self, hard=False):
        if hard:
            # Completely empty Rx queue, including packets received in the current simulation step
            self._rx_queue.clear()
        else:
            # Remove all old packets, but keep the ones received in the current simulation step
            new_packets = []

            for packet in self._rx_queue:
                if packet.timestamp >= self._sim.get_time():
                    new_packets.append(packet)

            # Overwrite Rx queue
            self._rx_queue = new_packets

    def update_reported_evts(self, report_all=False):
        if len(self._events_reported) > 0:
            logging.warning('Expected reported event list to be empty, but contained %i elements' % (len(self._events_reported),))
            return

        remaining_evts = []
        for event in self.events:
            # If event has been sampled long enough, we can send it off
            if event.get_active_time() > self._sample_duration_ms or report_all:
                self._events_reported.append(event)
            else:
                remaining_evts.append(event)

        if len(self.events) != len(self._events_reported) + len(remaining_evts):
            logging.error('Expected to split events into two separate queues, but corrupted data')
        else:
            self.events = remaining_evts
            if len(self._events_reported) > 0 or len(self.events) > 0:
                logging.debug('Node %i added %i events for reporting, %i events remain sampling at time %ius' % (self.node_id, len(self._events_reported), len(self.events), self._sim.get_time(),))

    def clear_reported_evts(self, reported):
        if len(self._events_reported) == 0:
            return
        elif reported:
            logging.debug('Node %i removed %i reported events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))
        else:
            logging.debug('Node %i removed %i suppressed events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))

        [event.terminate(reported) for event in self._events_reported]
        self._events_reported.clear()

    def clear_event_ids(self, event_ids):

        # Remove events from reported queue if they are not part of co-detections
        for event_id in event_ids:
            evt_removed = False
            for event in self._events_reported:
                if event.sequence_number() == event_id:

                    # Mark as not reported
                    event.terminate(reported=False)
                    self._events_reported.remove(event)

                    evt_removed = True
                    break

            if not evt_removed:
                logging.error('Node %i tried to remove event %i from reported queue, but event was not found' % (self.node_id, event_id,))

        if len(event_ids) > 0:
            logging.debug('Node %i removed %i events from reported queue which did not belong to a co-detection at time %ius' % (self.node_id, len(event_ids), self._sim.get_time(),))

    def observed_codet_events(self, timings):
        if timings is None or len(timings) == 0:
            return 0
        elif not isinstance(timings, list):
            logging.error('Expected to receive list of timings, but received %s' % (timings,))
            return 0

        # Sort timings
        timings.sort(key=itemgetter('timing'))

        nr_codet_evts = 0
        no_codet_evts = []
        i             = 0
        while i < len(timings):
            codet_end = i + 1
            while codet_end < len(timings) and timings[codet_end]['timing'] < (timings[i]['timing'] + MAX_DELTA_US):
                codet_end += 1

            codet_size = codet_end - i

            if codet_size >= self._sim.codet_thres:
                nr_codet_evts += codet_size
                # logging.debug('Node %i detected a co-detection of size %i at time %ius' % (self.node_id, codet_size, self._sim.get_time(),))
            else:
                # Remove local events from reported events
                for j in range(i, codet_end):
                    if timings[j]['node_id'] == self.node_id:
                        no_codet_evts.append(timings[j]['event_id'])

            # Set i to start of next co-detection
            i += codet_size

        # Remove local events not belonging to co-detections from the reported event queue, as they are suppressed
        if self._report_codets_only:
            self.clear_event_ids(no_codet_evts)

        if self._is_leader:
            logging.debug('Node %i detected %i events belonging to co-detections at time %ius' % (self.node_id, nr_codet_evts, self._sim.get_time(),))

        if self._report_codets_only:
            return nr_codet_evts
        else:
            # If a co-detection has been detected, report all events
            return len(timings) if nr_codet_evts > 0 else 0

    def received_schedule(self):
        return sum([packet.type == ELWBPacket.Type.SCHEDULE for packet in self._rx_queue]) > 0

    def get_nr_local_events(self):
        return len(self.events) + len(self._events_reported)

    def get_nr_elwb_requests(self):
        return sum([packet.type == ELWBPacket.Type.REQUEST for packet in self._rx_queue])

    def get_nr_elwb_events(self):
        return sum([packet.nr_events if packet.type == ELWBPacket.Type.DATA else 0 for packet in self._rx_queue])

    def get_local_event_timings(self):
        return [{'node_id': self.node_id, 'event_id': event.sequence_number(), 'timing': event.get_start_time()} for event in self._events_reported]

    def get_elwb_event_timings(self):
        evt_timings = []
        for packet in self._rx_queue:
            if packet.type == ELWBPacket.Type.DATA:
                evt_timings.extend(packet.evt_timings)

        return evt_timings

    def is_leader(self):
        return self._is_leader

    def make_leader(self):
        if self._is_leader:
            logging.warning('Tried to assign leader to node %i which is already elected as leader' % (self.node_id,))
        else:
            self._is_leader = True
            logging.debug('Node %i has been elected as leader' % (self.node_id,))


class ELWBPacket:

    class Type(Enum):
        INVALID  = 0,
        SCHEDULE = 1,
        REQUEST  = 2,
        DATA     = 3

    def __init__(self, sender, packet_type, leader=None, reporter=None, nr_events=0, evt_timings=None):
        self.sender    = sender
        self.type      = packet_type
        self.timestamp = self.sender._sim.get_time()

        self.leader      = leader
        self.reporter    = reporter
        self.nr_events   = nr_events
        self.evt_timings = evt_timings

        # Check packet
        if   self.type == ELWBPacket.Type.INVALID:
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == ELWBPacket.Type.SCHEDULE and (self.leader is     None or self.reporter is     None or nr_events > 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == ELWBPacket.Type.REQUEST  and (self.leader is not None or self.reporter is not None or nr_events == 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == ELWBPacket.Type.DATA     and (self.leader is not None or self.reporter is not None or nr_events == 0 or len(self.evt_timings) != self.nr_events):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
