#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation for the STeC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import os
import sys
import datetime as dt
import salabim as sim
import pandas as pd
from argparse import ArgumentParser
from random import choice, seed, randint
from IPython.display import display

sys.path.append("../")
sys.path.append("../../analysis")
from data_management.data_manager    import DataManager
from data_management.data_processing import DataProcessor
from des.des_aloha import ALOHANode
from des.des_ebc   import EBCNode
from des.des_elwb  import ELWBNode

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

DEFAULT_LOG_LEVEL = 'INFO'

DATE_FORMAT = '%d/%m/%Y'
TIME_FORMAT = '%H:%M:%S'

LOG_FORMAT      = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FORMAT = '%H:%M:%S'

FILE_DIR             = os.path.dirname(__file__)
DEFAULT_METRICS_FILE = FILE_DIR + '/metrics_'

# We simulate with us granularity
_US      = 1
MS_TO_US = 1000
S_TO_MS  = 1000
S_TO_US  = 1000 * 1000

# Simulation parameters
NR_IDS    = 32
NR_NODES  = 32
NR_EVENTS = 0

EXCLUDED_IDS = [21033, 21036]

DEFAULT_SPREADING_FACTOR   = 7
DEFAULT_SAMPLE_DURATION_MS = 100

ENABLE_LPM_COSTS = False

# Global constants
BASE_ID     = 21004
RANDOM_SEED = 7553

MAX_DELTA_US          = 100 * MS_TO_US
MIN_EVENT_DURATION_US =   2 * S_TO_US
MAX_EVENT_DURATION_US =  15 * S_TO_US

MAX_EVT_PROP_DURATION_MS = MAX_DELTA_US / MS_TO_US

CODET_NR_PDF_100MS  = [2883, 148, 56, 22, 15,  3, 2, 3, 1, 0, 1, 1, 1, 0, 0, 1]
CODET_NR_PDF_1000MS = [2434, 204, 87, 36, 17, 12, 4, 5, 6, 3, 1, 2, 0, 1, 0, 1]

CODET_TIME_PDF_100MS = [ [ 2, 1, 3, 2, 1, 4, 7, 5, 5, 6, 4, 1, 4, 4, 2, 0, 4, 5, 3, 4, 5, 8, 11, 9, 8, 1, 2, 3, 9, 7, 2, 3, 4, 2, 3, 2, 0, 1, 1, 2, 0, 3, 0, 0, 1, 0, 1, 0, 2, 2, 1, 2, 1, 0, 0, 3, 1, 0, 0, 3, 1, 2, 2, 3, 1, 3, 2, 1, 0, 3, 2, 3, 1, 4, 4, 3, 3, 6, 2, 3, 4, 1, 0, 2, 2, 3, 0, 5, 2, 0, 0, 2, 0, 0, 1, 1, 0, 1, 0, 1],
                         [-0.89817807, -0.83950458, -0.78083108, -0.72215759, -0.6634841, -0.6048106, -0.54613711, -0.48746362, -0.42879013, -0.37011663, -0.31144313, -0.25276964, -0.19409615, -0.13542266, -0.07674917, -0.01807567, 0.04059782, 0.09927131, 0.1579448,  0.2166183,  0.2752918,  0.33396529, 0.39263878, 0.45131227, 0.50998576,
                           0.56865926,  0.62733276,  0.68600624,  0.74467974,  0.80335323, 0.86202672, 0.92070022,  0.97937372,  1.03804721,  1.0967207,   1.15539419,  1.21406768,  1.27274118,  1.33141467,  1.39008816,  1.44876165, 1.50743515, 1.56610864, 1.62478214, 1.68345563, 1.74212912, 1.80080262, 1.85947611, 1.9181496,  1.97682309,
                           2.03549659,  2.09417009,  2.15284358,  2.21151707,  2.27019057, 2.32886406, 2.38753755,  2.44621105,  2.50488453,  2.56355802,  2.62223152,  2.68090502,  2.7395785,   2.798252,    2.8569255,   2.91559898, 2.97427248, 3.03294597, 3.09161946, 3.15029296, 3.20896645, 3.26763994, 3.32631344, 3.38498693, 3.44366043,
                           3.50233392,  3.56100741,  3.61968091,  3.6783544,   3.73702789, 3.79570139, 3.85437488,  3.91304837,  3.97172187,  4.03039535,  4.08906885,  4.14774235,  4.20641583,  4.26508933,  4.32376283,  4.38243631, 4.44110981, 4.49978331, 4.55845679, 4.61713029, 4.67580379, 4.73447727, 4.79315077, 4.85182426, 4.91049775]]

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class EventGenerator(sim.Component):

    def __init__(self, simulator, max_nr_events=0, trace=None, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.max_nr_events   = max_nr_events
        self.trace_offset_us = 0

        self._sim       = simulator
        self._nr_evts   = 0
        self._nr_codets = 0
        self._trace     = trace
        self._codet_ids = set()
        self._rep_ids   = set()

    def process(self):
        dist_codet_nr     = sim.Pdf(spec=range(2, len(CODET_NR_PDF_100MS) + 1), probabilities=CODET_NR_PDF_100MS[1:])
        dist_codet_time   = sim.Pdf(spec=CODET_TIME_PDF_100MS[1],               probabilities=CODET_TIME_PDF_100MS[0])
        dist_evt_prop     = sim.Uniform(0, MAX_DELTA_US)
        dist_evt_duration = sim.Uniform(MIN_EVENT_DURATION_US / S_TO_US, MAX_EVENT_DURATION_US / S_TO_US)

        curr_codet_time = 0

        while self._nr_codets < self.max_nr_events or self.max_nr_events == 0:

            curr_codet_nr = int(dist_codet_nr.sample())
            curr_evt      = [{'id': None, 'time': int(dist_evt_prop.sample()), 'duration': dist_evt_duration.sample()} for _ in range(curr_codet_nr)]

            yield from self.produce_codet(curr_codet_time, curr_codet_nr, curr_evt)

            # Schedule next co-detection time
            curr_codet_time += int(pow(10, dist_codet_time.sample()) * S_TO_US)

        # Allow remaining events to be properly processed
        yield self.hold(30 * S_TO_US)

        logging.info('Event generator stopped after having generated %i co-detections with a total of %i events within %ius' % (self._nr_codets, self._nr_evts, self._sim.get_time(),))

        # Stop simulation as no more events will be generated
        self._sim.stop_simulation()

    def process_trace(self, print_progress=True):
        if self._trace is None or not isinstance(self._trace, list):
            logging.error('Invalid trace input to generate events')
            return

        self.trace_offset_us = self._trace[0][0]['time']  # Note: We assume that the trace is ordered in time

        first_date = dt.datetime.fromtimestamp(self.trace_offset_us / S_TO_US)
        logging.info('Using trace data containing up to %i co-detections with an offset of %ius (original start date: %s)' % (len(self._trace), self.trace_offset_us, first_date.strftime(DATE_FORMAT),))
        if len(self._trace) < self.max_nr_events:
            logging.warning('Limiting generation of events from %i to %i due to limited number of events in available trace' % (self.max_nr_events, len(self._trace),))
            self.max_nr_events = len(self._trace)

        while self._nr_codets < self.max_nr_events or self.max_nr_events == 0:

            curr_codet            = self._trace.pop(0)
            curr_codet_offset     = curr_codet[0]['time']
            curr_codet[0]['time'] = 0  # Overwrite so that it can be directly used for the propagation delay

            curr_codet_time = curr_codet_offset - self.trace_offset_us
            curr_codet_nr   = len(curr_codet)
            curr_evt        = curr_codet

            yield from self.produce_codet(curr_codet_time, curr_codet_nr, curr_evt)

            # Show progress every 5%
            print_percentage = 5
            if print_progress and not (self._nr_codets % max(int(self.max_nr_events * print_percentage / 100), 1)):
                logging.info('Status of simulation progress: %3i%%' % (print_percentage * int(self._nr_codets / max(int(self.max_nr_events * print_percentage / 100), 1)),))
                logging.info('Simulated %8is so far (corresponds to %3i days)' % (curr_codet_time / S_TO_US, curr_codet_time / S_TO_US / 3600 / 24,))

        # Allow remaining events to be properly processed
        yield self.hold(30 * S_TO_US)

        logging.info('Event generator stopped after having generated %i co-detections with a total of %i events within %ius' % (self._nr_codets, self._nr_evts, self._sim.get_time(),))

        # Stop simulation as no more events will be generated
        self._sim.stop_simulation()

    def produce_codet(self, codet_time, codet_nr, evt):
        if not isinstance(evt, list) or len(evt) != codet_nr:
            logging.error('Received erroneous event statistics')
            return

        # Wait until the next event arrives
        time_delta = codet_time - self._sim.get_time()
        logging.debug('Waiting %4.3fs for next co-detection' % (time_delta / S_TO_US,))
        yield self.hold(till=codet_time)

        # Log IDs if it is a co-detection which reaches the threshold
        if codet_nr >= self._sim.codet_thres:
            self._codet_ids.update(range(self._nr_evts, self._nr_evts + codet_nr))

        # Generate a co-detection with the given characteristics
        self._nr_codets += 1
        self._nr_evts   += codet_nr
        logging.debug('Generating co-detection with %i %s at time %10.3fs (delta %8.3fs)' % (codet_nr, 'nodes' if codet_nr > 1 else 'node', self._sim.get_time() / S_TO_US, time_delta / S_TO_US,))

        # For each node, generate an event
        for i in range(codet_nr):
            curr_id          = evt[i]['id']
            curr_propagation = evt[i]['time']
            curr_duration    = int(evt[i]['duration'] * S_TO_US)  # Duration is stored in seconds

            event = Event(simulator=self._sim, duration_us=curr_duration, prop_us=curr_propagation, node_id=curr_id)
            logging.debug('Created new event %i with a sampling duration of %8ius at propagation offset %5ius for node %i' % (event.sequence_number(), curr_duration, curr_propagation, curr_id if curr_id is not None else 0,))

    def store_reported_evt(self, evt_id):
        self._rep_ids.add(evt_id)

    def get_evt_statistics(self, print_output=True):
        avg_evts = self._nr_evts / self._nr_codets if self._nr_codets > 0 else 0

        nr_codet_evts_tot   = len(self._codet_ids)
        nr_codet_evts_rep   = len(self._rep_ids.intersection(self._codet_ids))
        nr_evts_superfluous = len(self._rep_ids - self._rep_ids.intersection(self._codet_ids))

        if print_output:
            logging.info('Time: %is - Generated %i co-detections with a total of %i events (average %4.1f events / co-detection); reported %i out of %i events from co-detections (%6.2f%%)' % (self._sim.get_time() / S_TO_US, self._nr_codets, self._nr_evts, avg_evts, nr_codet_evts_rep, nr_codet_evts_tot, nr_codet_evts_rep / nr_codet_evts_tot * 100 if nr_codet_evts_tot > 0 else 100,))

        return {'nr_codets': self._nr_codets, 'nr_evts': self._nr_evts, 'nr_codet_evts_tot': nr_codet_evts_tot, 'nr_codet_evts_rep': nr_codet_evts_rep, 'nr_evts_superfluous': nr_evts_superfluous}


class Event(sim.Component):

    def __init__(self, simulator, duration_us=0, prop_us=0, node_id=None, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.duration_us = duration_us
        self.prop_us     = prop_us
        self.node_id     = node_id

        self._sim      = simulator
        self._start_us = self._sim.get_time() + self.prop_us

        # Do not include in trace
        self.suppress_trace(True)

    def process(self):
        self.enter(self._sim.get_evt_queue())

        # Wait for propagation
        yield self.hold(self.prop_us)

        if   self._sim.get_time() < self._start_us:
            logging.warning('Event %2i was activated at time %ius before its intended start date %ius (early by %ius)' % (self.sequence_number(), self._sim.get_time(), self._start_us, self._start_us - self._sim.get_time(),))
        elif self._sim.get_time() > self._start_us:
            logging.warning('Event %2i was activated at time %ius after its intended start date %ius (late by %ius)' % (self.sequence_number(), self._sim.get_time(), self._start_us, self._sim.get_time() - self._start_us,))

        # Find an inactive node
        if self.node_id is not None:
            logging.debug('Activating node %i after %ius propagation for %ius of sampling at time %ius' % (self.node_id, self.prop_us, self.duration_us, self._sim.get_time(),))
        self._sim.activate_node(event=self)

        yield self.passivate()

    def terminate(self, reported):
        if reported:
            self._sim.store_reported_evt(self.sequence_number())

        # Remove event from scheduled list
        self.activate()

    def get_start_time(self):
        return self._start_us

    def get_active_time(self):
        return self._sim.get_time() - self._start_us


class MetricGenerator(sim.Component):

    def __init__(self, simulator, period_us=1*3600*S_TO_US, window_size_us=6*3600*S_TO_US, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self._sim               = simulator
        self._period_us         = period_us
        self._window_size_us    = window_size_us
        self._metrics           = None
        self._metrics_list      = []
        self._meta_metrics_list = []
        self._post_processed = False

        # Do not include in trace
        self.suppress_trace(True)

    def process(self, print_progress=True):
        period_cnt = 0
        logging.info('Starting metric generation with file storage %s' % ('enabled' if self._sim.store_metrics else 'disabled',))

        while True:

            if self._post_processed:
                logging.error('Fetched metrics before simulation was over; aborting logging...')
                yield self.passivate()
                continue

            # Fetch next data sample
            curr_time    = self._sim.get_time()
            curr_metrics = self._sim.get_node_statistics(return_as_df=False)

            # Add current time to metrics
            for element in curr_metrics:
                element['time'] = curr_time

            # Add metrics to list
            self._metrics_list.extend(curr_metrics)

            # Wait for next sample interval
            yield self.hold(self._period_us)

            # Print update once a day
            if print_progress and not ((period_cnt * self._period_us) % (24*3600*S_TO_US)):
                logging.info('Simulated %8is so far (corresponds to %3i days)' % (self._sim.get_time() / S_TO_US, self._sim.get_time() / S_TO_US / 3600 / 24,))
            period_cnt += 1

    def store_codet_metric(self, codet_metric):
        self._meta_metrics_list.append(codet_metric)

    def generate_metric_trace(self):
        if self._post_processed:
            logging.warning('Already computed metric trace, skipping...')
            return

        # Generate a dataframe out of the list for performance reasons (list comprehension is much more performant than appending to dataframes)
        self._metrics = pd.DataFrame(self._metrics_list)

        # For each column which is aggregated throughout the simulation, we compute the difference
        columns_aggr  = ['nr_evts_tot', 'nr_evts_rep', 'energy_com_tot', 'energy_sens_tot', 'latency_tot', 'meas_tot']
        columns_delta = [column + '_delta' for column in columns_aggr]

        # Compute a rolling average
        columns_rolling_avg = [column + '_rolling_avg' for column in columns_aggr]
        rolling_window_size = max(int(self._window_size_us / self._period_us), 1)

        for node_id in self._sim.get_node_ids():
            mask = self._metrics['node_id'] == node_id

            # Deltas
            for i in range(len(columns_aggr)):
                self._metrics.loc[mask, columns_delta[i]] = self._metrics.loc[mask, columns_aggr[i]].diff(periods=-1).fillna(0).abs()

            # Store un-normalized COM energy per time
            self._metrics.loc[mask, 'energy_com_tot_delta_unnormalized'] = self._metrics.loc[mask, 'energy_com_tot_delta']

            # Normalization per event (requires computation of deltas first; skipping 'nr_evts_tot_delta' and 'nr_evts_rep_delta')
            for i in range(2, len(columns_aggr)):
                if columns_delta[i] == 'latency_tot_delta':
                    self._metrics.loc[mask, columns_delta[i]] = self._metrics.loc[mask, columns_delta[i]] / self._metrics.loc[mask, 'nr_evts_rep_delta'].replace(to_replace=0, value=1)
                else:
                    self._metrics.loc[mask, columns_delta[i]] = self._metrics.loc[mask, columns_delta[i]] / self._metrics.loc[mask, 'nr_evts_tot_delta'].replace(to_replace=0, value=1)

            # Rolling average
            for i in range(len(columns_aggr)):
                self._metrics.loc[mask, columns_rolling_avg[i]] = self._metrics.loc[mask, columns_delta[i]].rolling(window=rolling_window_size, min_periods=1, center=True).mean()

        self._post_processed = True

    def get_metrics(self):
        if not self.ispassive() and not self.isdata():
            logging.error('Metric generator is still running; stop the simulation first before fetching metrics')
            return None
        elif not self._post_processed:
            logging.warning('Post processing has not yet occurred; doing so now...')
            self.generate_metric_trace()

        return self._metrics

    def get_meta_metrics(self):
        if not self.ispassive() and not self.isdata():
            logging.error('Metric generator is still running; stop the simulation first before fetching metrics')
            return None
        else:
            return pd.DataFrame(self._meta_metrics_list)


class Simulator:

    def __init__(self):
        self.codet_thres = 1
        self.nr_ids      = 0
        self.base_id     = BASE_ID

        self._env              = None
        self._evt_generator    = None
        self._node_list        = None
        self._nr_nodes         = 0
        self._evt_queue        = None
        self._metric_generator = None

        # Options
        self._enable_lpm    = ENABLE_LPM_COSTS
        self._store_metrics = False

    def init_simulation(self, max_nr_events=0, nr_nodes=NR_NODES, nr_ids=NR_IDS, codet_threshold=2, print_trace=False, use_trace_data=False, use_real_data=False, trace_path=None, log_metrics=False, store_metrics=False):
        self.codet_thres = codet_threshold
        self.nr_ids      = nr_ids

        # Make sure we do not exceed the number of IDs
        if nr_nodes > self.nr_ids:
            logging.warning('Reducing number of nodes from %i to %i due to ID limitations' % (nr_nodes, self.nr_ids,))
            self._nr_nodes = self.nr_ids
        else:
            self._nr_nodes = nr_nodes

        # Create environment
        self._env = sim.Environment(trace=print_trace, random_seed=RANDOM_SEED)

        # Create event generator and event queue
        use_logs = False
        if use_trace_data or use_real_data:
            DataMgr      = DataManager(deployment='dirruhorn', config_file='../../analysis/stec.conf', project_name='stec', start_time=dt.datetime.strptime("01/06/2018", "%d/%m/%Y"), end_time=dt.datetime.strptime("01/01/2020", "%d/%m/%Y"))
            DataMgr_logs = DataManager(deployment='etz',       config_file='../../analysis/stec.conf', project_name='stec', start_time=dt.datetime.strptime("19/10/2020", "%d/%m/%Y"), end_time=dt.datetime.strptime("17/05/2021", "%d/%m/%Y"))
            DataProc     = DataProcessor(config_file='../../analysis/stec.conf', project_name='stec')

            if use_real_data:
                if use_trace_data:
                    logging.info('Simulating real trace')
                else:
                    logging.warning('Simulating real trace without trace data being enabled')

                if use_logs:
                    logging.info('Using log-based traces from ETZ deployment between %s - %s' % ('19/10/2020', '17/05/2020',))

                    # Get logged data and generate dataframe of co-detections
                    url_logs_test = DataMgr_logs.assemble_gsn_url('_dpp_geophone_acq_logs__conv')
                    df_logs_test  = DataMgr_logs.fetch_csv_data(url_logs_test, description='Logs')
                    trace = DataProc.generate_codetection_trace(df_logs_test)
                else:
                    # Filter deployment days and days with precipitation
                    excluded_dates = [['31/05/2017'], ['08/06/2017', '09/06/2017'], ['10/07/2017'], ['25/07/2017'], ['31/08/2017'], ['08/09/2017'], ['30/09/2017'], ['17/04/2018'], ['24/04/2018'], ['10/07/2018', '11/07/2018'], ['27/09/2018'], ['27/03/2019'], ['19/06/2019'], ['03/04/2020'], ['03/06/2020'], ['11/08/2020'], ['15/08/2020']]
                    include_rain   = False

                    if not include_rain:
                        prec_dates = DataMgr.fetch_rain_dates()
                        if len(prec_dates):
                            excluded_dates += prec_dates
                            logging.info("Added %d days to list of excluded dates due to precipitation" % (len(prec_dates),))

                    # Get trace data
                    cond_ids = [{'join': 'and', 'field': 'device_id', 'min': 21003, 'max': 21037}]
                    url      = DataMgr.assemble_gsn_url('_dpp_geophone_acq__conv', fields='device_id,start_time,end_time,generation_time,trg_duration', conditions=cond_ids)
                    data     = DataMgr.fetch_csv_data(url)
                    trace    = DataProc.generate_codetection_trace(data, dates_excluded=excluded_dates)
            else:
                if trace_path is None:
                    logging.error('Trace file not defined')

                logging.info('Simulating file-based trace')
                trace = DataMgr.load_codetection_trace(trace_path, id_offset=self.base_id)

            # Use event generator which replicates trace
            self._evt_generator = EventGenerator(simulator=self, name='event_generator', process='process_trace', max_nr_events=max_nr_events, trace=trace)
        else:
            # Use default event generator which creates events based on statistical data
            self._evt_generator = EventGenerator(simulator=self, name='event_generator', max_nr_events=max_nr_events)

        # Generate a queue to enter events
        self._evt_queue = sim.Queue('event_queue')

        # Generate metrics if desired
        if log_metrics:

            # Periodically sample all node statistics for tracing
            self._metric_generator = MetricGenerator(simulator=self)

            if store_metrics:
                self._store_metrics = True

        # Initialize RNG with seed
        seed(RANDOM_SEED)

    def start_simulation(self, duration_us):
        self._env.run(till=duration_us)

    def stop_simulation(self):

        # Make sure that all components have been stopped
        if self._evt_generator is not None:
            self._evt_generator.cancel()
        if self._node_list is not None:
            [node.terminate() for node in self._node_list]
        if self._evt_queue is not None:
            [event.cancel() for event in self._evt_queue]
        if self._metric_generator is not None:
            self._metric_generator.cancel()

        logging.info('Stopped all components%s' % (', simulation should terminate' if self._env.end_on_empty_eventlist else ''))

        # if len(self._env._event_list):
        #    logging.error('Event list of environment still contains %i events' % (len(self._env._event_list)))

        # Log metrics to file if desired
        if self._store_metrics:
            metrics = self.get_metrics(correct_trace_offset=True)
            self.store_metrics(metrics)

            metrics_meta = self.get_meta_metrics(correct_trace_offset=True)
            self.store_metrics(metrics_meta, postfix='_meta')

    def print_statistics(self):

        # Give detailed statistics on event queue
        # self._evt_queue.print_info()        # See queue state evolution
        # self._evt_queue.print_histograms()  # See queue histogram
        # self._evt_queue.print_statistics()  # See queue statistics (overall)

        # Print remaining events of the environment
        # self._env._print_event_list('Final')

        # Give statistics on event generation
        self._evt_generator.get_evt_statistics()

        # Give statistics on nodes
        stats = self.get_node_statistics()
        self.print_node_statistics(stats)

    def get_node_statistics(self, return_as_df=False):
        rows_list = []

        for node in self._node_list:
            energy_com_avg  = node.energy_mJ              / node.nr_evts_total    if node.nr_evts_total    > 0 else 0
            energy_sens_avg = node.sensor.energy_mJ       / node.nr_evts_total    if node.nr_evts_total    > 0 else 0
            latency_avg     = node.latency_us             / node.nr_evts_reported if node.nr_evts_reported > 0 else 0
            meas_avg        = node.sensor.measurements_ms / node.nr_evts_total    if node.nr_evts_total    > 0 else 0

            row = {'node_id': node.node_id, 'nr_evts_tot': node.nr_evts_total, 'nr_evts_rep': node.nr_evts_reported, 'energy_com_avg': energy_com_avg, 'energy_com_tot': node.energy_mJ, 'energy_sens_avg': energy_sens_avg, 'energy_sens_tot': node.sensor.energy_mJ, 'latency_avg': latency_avg, 'latency_tot': node.latency_us, 'meas_avg': meas_avg, 'meas_tot': node.sensor.measurements_ms}
            rows_list.append(row)

        if return_as_df:
            return pd.DataFrame(rows_list)
        else:
            return rows_list

    @staticmethod
    def print_node_statistics(list_stats, html_output=False):

        # Print node statistics
        df_columns = ['Node ID', 'Events [#]', 'Reported events [#]', 'Avg COM Energy [mJ]', 'Tot COM Energy [mJ]', 'Avg SENS Energy [mJ]', 'Tot SENS Energy [mJ]', 'Avg Latency [us]', 'Tot Latency [us]', 'Avg Meas Duration [ms]', 'Tot Meas Duration [ms]']

        str_base = '\nNode information:'

        if list_stats is None:
            if html_output:
                print('Invalid dataframe for node statistics')
            else:
                logging.info(str_base + '  Empty')
        else:

            if html_output:
                print('Number of nodes: {:d}'.format(len(list_stats)))
            else:
                str_base += '\n  Number of nodes: %d' % (len(list_stats),)
                str_base += '\nNode statistics:\n {0:s} | {1:s} | {2:s} | {3:s} | {4:s} | {5:s} | {6:s} | {7:s} | {8:s} | {9:s} | {10:s} |'.format(*df_columns)

                for row in list_stats:
                    str_base += '\n  {0:6d} | {1:10d} | {2:19d} |{3:20d} | {4:19d} | {5:20d} | {6:20d} | {7:16d} | {8:16d} | {9:22d} | {10:22d} |'.format(*[int(value) for value in row.values()])

            if html_output:
                node_stats         = pd.DataFrame(list_stats)
                node_stats.columns = df_columns
                display(node_stats)
            else:
                logging.info(str_base)

    def get_evt_statistics(self):
        return self._evt_generator.get_evt_statistics(print_output=False)

    def generate_network(self, node_type, spreading_factor=DEFAULT_SPREADING_FACTOR, sample_duration=DEFAULT_SAMPLE_DURATION_MS, period=None):
        if node_type is not EBCNode and node_type is not ELWBNode and node_type is not ALOHANode:
            logging.error('Tried to generate network of unsupported type %s' % (node_type.__name__ if node_type is not None else '[invalid]',))
            return

        # Generate communication nodes
        self._node_list = [node_type(simulator=self, node_id=self.generate_node_id(offset=self.base_id, iterator=i), spreading_factor=spreading_factor, sample_duration=sample_duration, enable_lpm_costs=self._enable_lpm, round_period_s=period) for i in range(self._nr_nodes)]

        if self.is_simulating_elwb():
            # Elect one node as leader (highest ID)
            self.assign_network_leader()

        # Print information on selectable options
        disabled_options_str = ''
        if not self._enable_lpm:
            disabled_options_str += '%sLow-Power Mode costs' % (', ' if len(disabled_options_str) > 0 else '',)
        if not self._store_metrics:
            disabled_options_str += '%sStoring of metrics' % (', ' if len(disabled_options_str) > 0 else '',)
        if len(disabled_options_str):
            logging.info('Disabled options: %s' % (disabled_options_str,))

        logging.info('Generated %i nodes of type %s' % (self._nr_nodes, node_type.__name__,))

    def generate_node_id(self, offset, iterator):
        curr_ID = offset  # ID if no ID would be excluded

        # Correct offset to make sure that base is used at least once
        curr_ID -= 1

        for j in range(iterator + 1):
            curr_ID += 1

            while curr_ID in EXCLUDED_IDS:
                curr_ID += 1

        # Verify that we do not create invalid IDs
        if curr_ID < self.base_id or self.base_id + (self.nr_ids + - 1) + len(EXCLUDED_IDS) < curr_ID or curr_ID in EXCLUDED_IDS:
            raise ValueError('Tried to initialize node with node ID %i, but simulation IDs must be in the interval %i - %i and not be one of %s' % (curr_ID, self.base_id, self.base_id + (self.nr_ids - 1), EXCLUDED_IDS,))

        return curr_ID

    def activate_node(self, event, random=False):

        # Select a random node, or always start with the lowest available ID
        if random:
            if event.node_id is not None:
                logging.warning('Ignoring activation for node ID %i, as random activation enabled' % (event.node_id,))

            node = choice(self._node_list)
            while not node.sensor.ispassive() and not node.sensor.isdata():
                node = choice(self._node_list)

            # Assign event to node and activate
            node.sensor.assign_event(event)

        else:
            if event.node_id is not None:
                # Search for specified node and activate it
                if event.node_id not in self.get_node_ids():
                    logging.warning('Ignoring activation for node ID %i, as not in list of nodes' % (event.node_id,))
                    return

                for node in self._node_list:
                    if node.node_id == event.node_id:
                        if node.sensor.ispassive() or node.sensor.isdata():
                            node.sensor.assign_event(event)
                            return
                        else:
                            logging.warning('Node %i which is to be activated is not passive (%6.3fs of sampling remaining)' % (node.node_id, node.sensor.get_remaining_time() / S_TO_US,))
            else:
                # Select the first node which is not already activated
                for node in self._node_list:
                    if node.sensor.ispassive() or node.sensor.isdata():
                        node.sensor.assign_event(event)
                        return

            logging.error('Could not find a suitable node to activate for event %i at time %ius' % (event.sequence_number(), self.get_time(),))

    def broadcast_packet(self, packet, add_tx_to_own_queue=True, activate=False):
        # logging.debug('Node %i is broadcasting a packet of type %s to all nodes %s itself and %s other nodes' % (packet.sender.node_id, packet.type.name, 'including' if add_tx_to_own_queue else 'excluding', 'activates' if activate else 'does not activate'))

        for node in self._node_list:
            if not add_tx_to_own_queue and packet.sender.node_id == node.node_id:
                pass  # Ignore packets that are sent by this node
            else:
                node.rx(packet, activate=activate)

    def assign_network_leader(self):
        if not self.is_simulating_elwb():
            logging.error('Tried to assign leader for network type that does not have one')
            return

        leader_ID = max(self.get_node_ids())

        for node in self._node_list:
            if node.node_id == leader_ID:
                node.make_leader()
                return

    def get_node_ids(self):
        return [node.node_id for node in self._node_list]

    def get_nr_reporters(self):

        # NOTE: This violates the concept of DES components, but cannot be solved efficiently without periodic beaconing
        return sum([node.is_reporting() for node in self._node_list])

    def get_evt_queue(self):
        return self._evt_queue

    def is_evt_queue_empty(self):
        return len(self._evt_queue) == 0

    def evt_queue_pop(self, event):
        if event is None:
            logging.error('Tried to remove invalid event from the event queue')
        elif event.node_id is None:
            logging.error('Tried to remove event %i from event queue even though it was not associated with any sensor' % (event.sequence_number(),))
        elif self._evt_queue.index(event) == -1:
            logging.error('Tried to remove event %i from event queue even though it was not in queue' % (event.sequence_number(),))
        else:
            self._evt_queue.remove(event)

    def get_time(self):
        return self._env.now()  # Returns the current time in microseconds

    def is_simulating_ebc(self):
        return isinstance(self._node_list[0], EBCNode)

    def is_simulating_elwb(self):
        return isinstance(self._node_list[0], ELWBNode)

    def is_simulating_aloha(self):
        return isinstance(self._node_list[0], ALOHANode)

    def store_reported_evt(self, evt_id):
        self._evt_generator.store_reported_evt(evt_id)

    def store_codet_metric(self, codet_metric):
        if self._metric_generator is not None:
            # If metrics generator exists (i.e. metrics should be stored), do so
            self._metric_generator.store_codet_metric(codet_metric)

    def get_metrics(self, correct_trace_offset=True):

        # Add differential data and rolling average
        self._metric_generator.generate_metric_trace()
        metrics = self._metric_generator.get_metrics().copy()

        if correct_trace_offset:
            first_date = dt.datetime.fromtimestamp(self._evt_generator.trace_offset_us / S_TO_US)
            logging.info('Correcting generated metrics with a start time of %ius (start date: %s)' % (self._evt_generator.trace_offset_us, first_date.strftime(DATE_FORMAT),))

            metrics['time_original'] = metrics['time'].add(self._evt_generator.trace_offset_us)

        return metrics

    def get_meta_metrics(self, correct_trace_offset=True):

        # Get dataframe of all reported co-detections
        meta_metrics = self._metric_generator.get_meta_metrics()

        if correct_trace_offset and meta_metrics.shape[0] > 0:
            first_date = dt.datetime.fromtimestamp(self._evt_generator.trace_offset_us / S_TO_US)
            logging.info('Correcting generated metrics with a start time of %ius (start date: %s)' % (self._evt_generator.trace_offset_us, first_date.strftime(DATE_FORMAT),))

            meta_metrics['time_original'] = meta_metrics['time'].add(self._evt_generator.trace_offset_us)

        return meta_metrics

    def store_metrics(self, df, postfix=''):
        if df is None or not isinstance(df, pd.DataFrame):
            raise TypeError('Invalid dataframe to store as .pkl')
        else:
            if   self.is_simulating_ebc():
                file_ending = 'ebc'   + postfix + '.pkl'
            elif self.is_simulating_elwb():
                file_ending = 'elwb'  + postfix + '.pkl'
            elif self.is_simulating_aloha():
                file_ending = 'aloha' + postfix + '.pkl'
            else:
                raise ValueError('Unknown network type')

            file_path = DEFAULT_METRICS_FILE + file_ending

            # Verify that file doesnt exist yet; if it does, overwrite
            if os.path.isfile(file_path):
                os.remove(file_path)
                logging.info('Removed old simulation file at path %s' % (file_path,))

            # Store dataframe
            df.to_pickle(path=file_path)
            logging.info('Saved metrics to path %s' % (file_path,))

    @staticmethod
    def load_metrics(load_ebc=False, load_elwb=False, load_aloha=False, postfix=''):
        if   load_ebc:
            file_ending = 'ebc'   + postfix + '.pkl'
        elif load_elwb:
            file_ending = 'elwb'  + postfix + '.pkl'
        elif load_aloha:
            file_ending = 'aloha' + postfix + '.pkl'
        else:
            raise ValueError('Unknown network type')

        file_path = DEFAULT_METRICS_FILE + file_ending

        # Verify that file does exist
        if not os.path.isfile(file_path):
            raise ValueError('Invalid path to load metrics: %s' % (file_path,))

        # Load dataframe
        df = pd.read_pickle(file_path)
        logging.info('Loaded metrics from path %s' % (file_path,))

        return df

# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-a', '--aloha', dest='simulate_aloha', action='store_true',
                            help='Simulate the ALOHA protocol.')
        parser.add_argument('-b', '--elwb',  dest='simulate_elwb', action='store_true',
                            help='Simulate the eLWB protocol.')
        parser.add_argument('-c', '--ebc', dest='simulate_ebc', action='store_true',
                            help='Simulate the STeC protocol.')
        parser.add_argument('-t', '--trace', dest='print_trace', action='store_true',
                            help='Print the simulation trace.')
        parser.add_argument('-s', '--store', dest='store_metrics', action='store_true',
                            help='Store the resulting metrics as a file.')
        parser.add_argument('-r', '--real-data', dest='use_real_data', action='store_true',
                            help='Use real trace data from the deployment as input.')
        parser.add_argument('-e', '--events', type=int, dest='nr_evts', default=NR_EVENTS,
                            help='Number of simulated events (default: %d).' % (NR_EVENTS,), metavar='INT')
        parser.add_argument('-d', '--debug-print', dest='print_debug', action='store_true',
                            help='Change printing level to DEBUG (default: %s).' % (DEFAULT_LOG_LEVEL,))
        cli_args = parser.parse_args()

    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Apply printing settings
    if cli_args.print_debug:
        DEFAULT_LOG_LEVEL = 'DEBUG'

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    # Pipe root logger to stdout (StreamHandler)
    handler = logging.StreamHandler()
    handler.setLevel(DEFAULT_LOG_LEVEL)
    handler.setFormatter(formatter)
    root.addHandler(handler)

    # Print debug options
    logging.debug('DES CLI options:')
    for arg in vars(cli_args):
        logging.debug(' {}:\t {}'.format(arg, getattr(cli_args, arg) or ''))

    logging.debug('Discrete Event Simulation script is running...')

    try:

        # Generate events (either upper-bounded by number of events or time)
        nr_events = cli_args.nr_evts
        if nr_events:
            sim_duration_us = None  # No time bound
            logging.info('Simulating %i %s' % (nr_events, 'co-detections' if nr_events > 1 else 'co-detection',))
        else:
            sim_duration_us = 1 * 24 * 3600 * S_TO_US  # Simulate one day by default
            logging.info('Simulating %is of run-time' % (sim_duration_us / S_TO_US,))

        # Generate simulator
        des_sim = Simulator()
        des_sim.init_simulation(max_nr_events=nr_events, print_trace=cli_args.print_trace, use_trace_data=cli_args.use_real_data, use_real_data=cli_args.use_real_data, log_metrics=cli_args.store_metrics, store_metrics=cli_args.store_metrics)

        if cli_args.simulate_ebc:
            des_sim.generate_network(EBCNode)
        elif cli_args.simulate_elwb:
            des_sim.generate_network(ELWBNode)
        elif cli_args.simulate_aloha:
            des_sim.generate_network(ALOHANode)
        else:
            raise ValueError('Must specify node type (no valid CLI option found)')

        # Run environment
        des_sim.start_simulation(sim_duration_us)

        # Print statistics
        des_sim.print_statistics()

    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
