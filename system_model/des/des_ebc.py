#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation of the STeC protocol for the STeC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import salabim as sim
from enum import Enum
from math import ceil, floor, log2
from random import choice, seed, randint

from des.des_sensor import GeophoneSensor

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# We simulate with us granularity
_US      = 1
MS_TO_US = 1000
S_TO_MS  = 1000
S_TO_US  = 1000 * 1000

# Global constants
NR_WAKEUP_SYMBOLS         = 2
NR_DISCOSYNC_SYMBOLS      = 9
NR_RP_TRANS_OPPORTUNITIES = 3

MAX_DELTA_US          = 100 * MS_TO_US
MIN_EVENT_DURATION_US =   2 * S_TO_US
MAX_EVENT_DURATION_US =  15 * S_TO_US

SAMPLE_DURATION_MS = 100

# Simulation artefacts
EBC_SW_EARLY_WAKEUP_US = 1
EBC_DA_EARLY_WAKEUP_US = 2

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class EBCNode(sim.Component):

    class State(Enum):
        INIT             = 0,
        SLEEP            = 1,
        STAG_WAKEUP      = 2,
        LEADER_ELECTION  = 3,
        DATA_AGGREGATION = 4,
        REPORTING        = 5,
        ACK_DISTRIBUTION = 6

    def __init__(self, simulator, node_id=0, spreading_factor=7, sample_duration=SAMPLE_DURATION_MS, enable_lpm_costs=True, round_period_s=None, enable_discosync=True, enable_AD=False, enable_sens_abort=False, report_codets_only=True, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.node_id          = node_id
        self.energy_mJ        = 0
        self.latency_us       = 0
        self.events           = []
        self.nr_evts_total    = 0
        self.nr_evts_reported = 0
        self.sensor           = GeophoneSensor(simulator=simulator, process='', parent_node=self, enable_lpm_costs=enable_lpm_costs)

        self._sim                = simulator
        self._state              = self.State.INIT
        self._rx_queue           = []
        self._is_receiving       = False
        self._last_evt_start     = 0
        self._curr_phase_start   = self._sim.get_time()
        self._curr_phase_end     = 0
        self._curr_phase_energy  = 0
        self._curr_exchange_time = 0
        self._next_da_start      = 0
        self._events_reported    = []
        self._spreading_factor   = spreading_factor
        self._sample_duration_ms = sample_duration
        self._enable_lpm_costs   = enable_lpm_costs
        self._enable_discosync   = enable_discosync
        self._enable_AD          = enable_AD
        self._enable_sens_abort  = enable_sens_abort
        self._report_codets_only = report_codets_only

        # Check that no wrong settings have been selected by command line
        if round_period_s is not None:
            logging.warning('Round period should not be set for STeC, but is set to %ss' % (round_period_s,))

    def process(self):

        # Initialize parameters
        LPM_mW = 0.010485 if self._enable_lpm_costs else 0

        sw_rx_mW                  = 32.297273
        sw_energy_transition_mJ   = 0.454304
        ds_energy_rx_mJ           = 0.076748
        ds_energy_tx_mJ           = 0.172722
        sw_duration_transition_ms = 13.17
        ds_duration_slot_ms       = 2.27

        le_energy_transition_mJ   = 0.972955
        le_energy_slot_rx_mJ      = 1.267412
        le_energy_slot_retrans_mJ = 0.892855
        le_energy_slot_tx_mJ      = 0.810045
        le_duration_transition_ms = 50.49 if self._enable_discosync else 7.17
        le_duration_slot_ms       = 28.00

        da_energy_sched_retrans_mJ = 1.530920
        da_energy_sched_tx_mJ      = 1.322059
        da_energy_slot_rx_mJ       = 1.180825
        da_energy_slot_retrans_mJ  = 2.537448
        da_energy_slot_tx_mJ       = 2.443399
        da_duration_transition_ms  = min(self._sample_duration_ms, int(MAX_EVENT_DURATION_US / MS_TO_US))
        da_duration_sched_ms       = 34.81
        da_duration_slot_ms        = 25.03

        rp_nr_retransmissions     = 0
        rp_duration_transition_ms = 23.78  # Partly randomized in practice

        ad_energy_comm_mJ      = 2.893299
        ad_req_slot_rx_mJ      = 1.209538
        ad_req_slot_retrans_mJ = 0.892855  # Use same value as for le_energy_slot_retrans_mJ (similar packet duration)
        ad_duration_comm_ms    = 62.09

        if self._spreading_factor == 7:
            health_msg_mJ = 27.622493

            rp_energy_overhead_mJ   = 21.076502
            rp_energy_per_node_mJ   = 5.242046
            rp_duration_overhead_ms = 149.26
            rp_duration_per_node_ms = 18.82

            ad_duration_transition_ms = 1599.61  # 7809.64ms if using SF10 as fallback
        elif self._spreading_factor == 10:
            health_msg_mJ = 161.664594

            rp_energy_overhead_mJ   = 115.437542
            rp_energy_per_node_mJ   = 33.22107
            rp_duration_overhead_ms = 796.51
            rp_duration_per_node_ms = 111.00

            ad_duration_transition_ms = 7809.80
        elif self._spreading_factor == 11:
            health_msg_mJ = None  # TODO: Measure

            rp_energy_overhead_mJ   = 254.915094
            rp_energy_per_node_mJ   = 66.369789
            rp_duration_overhead_ms = 1652.69
            rp_duration_per_node_ms = 220.23

            ad_duration_transition_ms = 16275.48
        else:
            raise ValueError('Unsupported spreading factor %i' % (self._spreading_factor,))

        # Start of the state machine
        while True:

            prev_state = self._state  # Store state for debugging
            prev_time  = self._sim.get_time()

            if self._state == self.State.INIT:

                # Wait for a sensor trigger
                self._curr_phase_start = self._sim.get_time()
                self._state            = self.State.SLEEP
                yield self.passivate()

            elif self._state == self.State.SLEEP:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor' % (self.node_id, event.sequence_number(),))

                    # Start communication
                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Update metrics
                    self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

                    # Action
                    self.start_rx()

                    # Discosync
                    discosync_offset_us = EBC_SW_EARLY_WAKEUP_US if self._enable_discosync else 0

                    self._curr_phase_start = self._last_evt_start
                    self._curr_phase_end   = self._sim.get_time() + MAX_DELTA_US - discosync_offset_us
                    self._state            = self.State.STAG_WAKEUP
                    yield self.hold(till=self._curr_phase_end)
                else:
                    logging.warning('Node %i activated without sensor trigger' % (self.node_id,))
                    yield self.passivate()

            elif self._state == self.State.STAG_WAKEUP:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.STAG_WAKEUP.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    yield self.hold(till=self._curr_phase_end)
                elif self.get_nr_wakeup_messages() == 0:
                    if self._sim.get_time() >= self._curr_phase_end:
                        event = self.events[0]  # Note: Event might not be running anymore, hence get it from the queue and not from the sensor directly
                        logging.debug('Node %i woke up as first node from current co-detection due to event %i at time %ius' % (self.node_id, event.sequence_number(), self._sim.get_time(),))

                        # Send wake-up message (will also be received by node itself)
                        self._sim.broadcast_packet(EBCPacket(sender=self, packet_type=EBCPacket.Type.WAKEUP), activate=True)
                    else:
                        logging.warning('Node %i woke up despite still being in STAG_WAKEUP for %ius' % (self.node_id, self._curr_phase_end - self._sim.get_time(),))
                        yield self.hold(till=self._curr_phase_end)
                elif self.send_discosync():
                    # Send Discosync message
                    self._sim.broadcast_packet(EBCPacket(sender=self, packet_type=EBCPacket.Type.DISCOSYNC))

                    # Wait for it to be received
                    self._curr_phase_end = self._sim.get_time() + EBC_SW_EARLY_WAKEUP_US
                    yield self.hold(till=self._curr_phase_end)

                    # Make sure nothing triggers node too early
                    while self._sim.get_time() < self._curr_phase_end:
                        logging.warning('Node %i woke up too early for leader election planned at %ius at time %ius' % (self.node_id, self._curr_phase_end, self._sim.get_time(),))
                        yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += elapsed_time_us / S_TO_US * sw_rx_mW + sw_energy_transition_mJ

                    if   self.get_nr_wakeup_messages() > 1:
                        logging.warning('Node %i received %i wakeup messages at time %ius' % (self.node_id, self.get_nr_wakeup_messages(), self._sim.get_time(),))
                    elif self.get_nr_wakeup_messages() < 1:
                        logging.warning('Node %i did not receive a wakeup message but %i other messages at time %ius' % (self.node_id, len(self._rx_queue), self._sim.get_time(),))

                    # Wakeup signal - included in LE for simulation
                    if self._enable_discosync:
                        sw_duration_ms = NR_DISCOSYNC_SYMBOLS * ds_duration_slot_ms + sw_duration_transition_ms
                        sw_energy_mJ   = floor(NR_DISCOSYNC_SYMBOLS/2) * ds_energy_tx_mJ + ceil(NR_DISCOSYNC_SYMBOLS/2) * ds_energy_rx_mJ
                    else:
                        sw_duration_ms = NR_WAKEUP_SYMBOLS * ds_duration_slot_ms + sw_duration_transition_ms
                        sw_energy_mJ   = NR_WAKEUP_SYMBOLS * ds_energy_tx_mJ
                    self.energy_mJ += sw_energy_mJ  # Already include here, as phase has just ended - time will still be included in LE for simulation

                    # Leader Election
                    nr_bits  = ceil(log2(self._sim.nr_ids))
                    set_bits = bin(self.node_id).count('1')

                    le_duration_ms = nr_bits  * le_duration_slot_ms  + le_duration_transition_ms
                    le_energy_mJ   = set_bits * le_energy_slot_tx_mJ + (nr_bits - set_bits) * le_energy_slot_rx_mJ + le_energy_transition_mJ

                    # Wait for maximal event duration - included in LE for simulation
                    da_energy_transition_mJ = da_duration_transition_ms / S_TO_MS * LPM_mW

                    # Action
                    da_delay_ms = max(da_duration_transition_ms, int(sw_duration_ms + le_duration_ms))  # Wait for at least the end of leader election, but at most until we have sampled enough
                    self._next_da_start = self._sim.get_time() + da_delay_ms * MS_TO_US

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = le_energy_mJ + da_energy_transition_mJ

                    if self.get_nr_discosync_messages() > 1 or not self._enable_discosync:
                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._next_da_start - EBC_DA_EARLY_WAKEUP_US
                        self._state            = self.State.LEADER_ELECTION
                        yield self.hold(till=self._curr_phase_end)
                    else:
                        logging.debug('Node %i did not detect any other active nodes after SW through DiscoSync at time %ius' % (self.node_id, self._sim.get_time(),))

                        # Abort sampling if enabled
                        if self._enable_sens_abort:
                            self.sensor.abort()

                        # Prepare events to be removed
                        self.update_reported_evts(report_all=True)

                        # Update reporting latency
                        if len(self._events_reported) > 0:
                            # Remove reported events to avoid adding them to the statistics
                            self.clear_reported_evts(reported=False)
                        else:
                            logging.warning('Node %i entered DiscoSync without any event to report at time %ius' % (self.node_id, self._sim.get_time(),))

                        # Make sure that LE costs are not counted
                        self._curr_phase_energy = 0

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._sim.get_time() + sw_duration_ms
                        self._state            = self.State.ACK_DISTRIBUTION
                        yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.LEADER_ELECTION:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.LEADER_ELECTION.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                elif self._curr_phase_end <= self._sim.get_time() < self._next_da_start:

                    # Action
                    self.clear_rx_queue(hard=False)
                    self._sim.broadcast_packet(EBCPacket(sender=self, packet_type=EBCPacket.Type.LEADER_ELECTION))
                    self._curr_exchange_time = self._sim.get_time()

                    self._curr_phase_end = self._next_da_start

                    # Wait for all nodes to have received each others packet, simulating leader election
                    yield self.hold(duration=EBC_DA_EARLY_WAKEUP_US / 2)

                    # Make sure nothing triggers node too early
                    while self._sim.get_time() < (self._curr_phase_end - EBC_DA_EARLY_WAKEUP_US / 2):
                        logging.warning('Node %i woke up too early for schedule distribution planned at %ius at time %ius' % (self.node_id, (self._curr_phase_end - EBC_DA_EARLY_WAKEUP_US / 2), self._sim.get_time(),))
                        yield self.hold(till=self._curr_phase_end - EBC_DA_EARLY_WAKEUP_US / 2)

                    if self.is_curr_leader(self._curr_exchange_time):
                        # If leader, broadcast schedule
                        curr_reporter = choice(self.get_eligible_nodes(self._curr_exchange_time))
                        self._sim.broadcast_packet(EBCPacket(sender=self, packet_type=EBCPacket.Type.DATA, leader=self.node_id, reporter=curr_reporter, nr_events=1))
                        logging.debug('Leader %i elected node %i as reporter for current network at time %ius' % (self.node_id, curr_reporter, self._sim.get_time(),))
                    else:
                        # If not leader, send data
                        self._sim.broadcast_packet(EBCPacket(sender=self, packet_type=EBCPacket.Type.DATA, nr_events=1))

                    self._curr_exchange_time = self._sim.get_time()
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Report events
                    self.update_reported_evts()

                    if len(self._events_reported) > 1:
                        logging.debug('Node %i is going to report aggregated information on %i events at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))
                    elif len(self._events_reported) < 1:
                        logging.warning('Node %i entered DA without any events to report at time %ius' % (self.node_id, self._sim.get_time(),))

                    # Data Aggregation
                    curr_nr_events = self.get_nr_ebc_events(self._curr_exchange_time)

                    da_duration_ms = da_duration_sched_ms  + self._sim.nr_ids * da_duration_slot_ms
                    da_energy_mJ   = da_energy_sched_tx_mJ + curr_nr_events   * da_energy_slot_tx_mJ + (self._sim.nr_ids - curr_nr_events) * da_energy_slot_rx_mJ

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = da_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + da_duration_ms * MS_TO_US
                    self._state            = self.State.DATA_AGGREGATION
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.DATA_AGGREGATION:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.DATA_AGGREGATION.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Reporting
                    curr_nr_events = self.get_nr_ebc_events(self._curr_exchange_time)

                    rp_duration_slot_ms = rp_duration_overhead_ms + curr_nr_events * rp_duration_per_node_ms
                    rp_duration_ms      = (1 + rp_nr_retransmissions) * rp_duration_slot_ms + rp_duration_transition_ms

                    rp_energy_slot_mJ = rp_energy_overhead_mJ + curr_nr_events * rp_energy_per_node_mJ
                    rp_energy_mJ      = (1 + rp_nr_retransmissions) * rp_energy_slot_mJ

                    # Only count reporting costs if applicable
                    if curr_nr_events < self._sim.codet_thres:
                        logging.debug('Node %i is ignoring reporting for network of %i nodes, as below co-detection threshold of %i' % (self.node_id, curr_nr_events, self._sim.codet_thres,))
                        rp_duration_ms = 0  # Filter if no co-detection
                        rp_energy_mJ   = 0

                        # Abort sampling if enabled
                        if self._enable_sens_abort:
                            self.sensor.abort()

                        # Remove reported events to avoid adding them to the statistics
                        self.clear_reported_evts(reported=False)
                    elif not self.is_curr_reporter(self._curr_exchange_time):
                        # NOTE: Because the latency computation is based on the duration of the reporting, we count the reporting time for all nodes (including nodes which do not report), but do not include the costs
                        # rp_duration_ms = 0

                        # Do not pay reporter costs if this node is not assigned to report
                        rp_energy_mJ = 0
                    else:
                        logging.debug('Node %i is reporting co-detection of %i nodes' % (self.node_id, curr_nr_events,))

                        # Add to meta metrics
                        codet_metric = {'time': self._sim.get_time(), 'reporter': self.node_id, 'codet_nodes': curr_nr_events}
                        self._sim.store_codet_metric(codet_metric)

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = rp_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + rp_duration_ms * MS_TO_US
                    self._state            = self.State.REPORTING
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.REPORTING:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.REPORTING.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Update reporting statistics (if events have been reported)
                    for event in self._events_reported:
                        self.latency_us       += self._sim.get_time() - event.get_start_time()
                        self.nr_evts_reported += 1

                    # Ack Distribution
                    corrected_transition_ms = ad_duration_transition_ms - elapsed_time_us / MS_TO_US  # Subtract time spent in reporting, as AD transition is in parallel
                    if corrected_transition_ms < 0:
                        logging.warning('Node %i experienced invalid transition from RP to AD with %ius in-between' % (self.node_id, corrected_transition_ms * MS_TO_US,))

                    ad_duration_ms = ad_duration_comm_ms + corrected_transition_ms
                    ad_energy_mJ   = ad_energy_comm_mJ   + corrected_transition_ms / S_TO_MS * LPM_mW

                    # Filter if single network or no co-detection (and hence no reporting)
                    if self.get_nr_ebc_events(self._curr_exchange_time) <= 1 or self.get_nr_ebc_events(self._curr_exchange_time) < self._sim.codet_thres:
                        ad_duration_ms = 0
                        ad_energy_mJ   = 0
                    elif not self._enable_AD:
                        ad_duration_ms = 0
                        ad_energy_mJ   = 0

                    # Store prospective energy costs during this phase
                    self._curr_phase_energy = ad_energy_mJ

                    self._curr_phase_start = self._sim.get_time()
                    self._curr_phase_end   = self._sim.get_time() + ad_duration_ms * MS_TO_US
                    self._state            = self.State.ACK_DISTRIBUTION
                    yield self.hold(till=self._curr_phase_end)

            elif self._state == self.State.ACK_DISTRIBUTION:

                if self.sensor.triggered():
                    # Get sensor event
                    event = self.sensor.get_event()
                    logging.debug('Node %i received event %2i from its sensor while in state %s' % (self.node_id, event.sequence_number(), self.State.ACK_DISTRIBUTION.name,))

                    self.events.append(event)
                    self.nr_evts_total  += 1
                    self._last_evt_start = self._sim.get_time()

                    # Wait for the end of the phase
                    yield self.hold(till=self._curr_phase_end)
                else:
                    # Update metrics
                    elapsed_time_us  = self._sim.get_time() - self._curr_phase_start
                    self.energy_mJ  += self._curr_phase_energy

                    # Remove all events associated with this node
                    self.clear_reported_evts(reported=True)

                    # Prepare state for next round
                    self._curr_phase_energy  = 0
                    self._curr_exchange_time = 0
                    self.clear_rx_queue(hard=True)

                    if len(self.events) > 0 and self._enable_AD and not self._report_codets_only:
                        additional_da_offset_ms = da_duration_transition_ms
                        logging.debug('Node %i observed %i events during communication; scheduling additional DA in %ims' % (self.node_id, len(self.events), additional_da_offset_ms,))

                        # Add prospective energy - need to correct AD estimate as it originally includes an Rx Slot
                        self._curr_phase_energy = (ad_req_slot_retrans_mJ - ad_req_slot_rx_mJ) + additional_da_offset_ms / S_TO_MS * LPM_mW

                        self._next_da_start = self._sim.get_time() + additional_da_offset_ms * MS_TO_US

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = self._next_da_start - EBC_DA_EARLY_WAKEUP_US
                        self._state            = self.State.LEADER_ELECTION
                        yield self.hold(till=self._curr_phase_end)
                    else:
                        # Remove all events which could not be sent to the base as they occurred during communication
                        if len(self.events) > 0:
                            # Clear event queues
                            logging.debug('Node %i discarded %i events which occurred during communication at time %ius' % (self.node_id, len(self.events), self._sim.get_time(),))
                            self.update_reported_evts(report_all=True)
                            self.clear_reported_evts(reported=False)

                        # Reset state
                        self.stop_rx()
                        self._next_da_start = 0

                        self._curr_phase_start = self._sim.get_time()
                        self._curr_phase_end   = 0
                        self._state            = self.State.SLEEP
                        yield self.passivate()

            logging.debug('Node %i switched from state %16s to state %16s at time %ius' % (self.node_id, prev_state.name, self._state.name, prev_time,))

    def terminate(self):

        # Stop sensor
        self.sensor.terminate()

        # Include LPM costs since last phase
        if self._enable_lpm_costs:
            LPM_mW = 0.010485

            self.energy_mJ += (self._sim.get_time() - self._curr_phase_start) / S_TO_US * LPM_mW

        # Stop itself
        self.cancel()

    def is_active(self):
        return self._state > self.State.SLEEP

    def is_reporting(self):
        return self._state == self.State.REPORTING

    def is_curr_reporter(self, time):
        curr_reporter = []

        for packet in self._rx_queue:
            if packet.type == EBCPacket.Type.DATA and packet.leader is not None and packet.reporter is not None and packet.timestamp == time:
                # logging.debug('Node %i observes that leader %i assigned node %i as reporter at time %ius' % (self.node_id, packet.leader, packet.reporter, time,))
                curr_reporter.append(packet.reporter)

        if   len(curr_reporter) == 1:
            return curr_reporter[0] == self.node_id
        elif len(curr_reporter) == 0:
            logging.error('Node %i did not observe any reporters assigned for its network at time %ius' % (self.node_id, time,))
            return False
        else:
            logging.error('Node %i observes multiple reporters assigned for same network (%s) at time %ius' % (self.node_id, curr_reporter, time,))
            return False

    def is_curr_leader(self, time):
        return max(self.get_eligible_nodes(time)) == self.node_id

    def get_eligible_nodes(self, time):
        curr_eligible_ids = []

        for packet in self._rx_queue:
            if packet.type == EBCPacket.Type.LEADER_ELECTION and packet.timestamp == time:
                curr_eligible_ids.append(packet.sender.node_id)

        return curr_eligible_ids

    def start_rx(self):
        if self._is_receiving:
            logging.warning('Node %i is already receiving' % (self.node_id,))
        elif len(self._rx_queue) > 0:
            logging.warning('Node %i tried to start receiving, but Rx queue already contains %i elements' % (self.node_id, len(self._rx_queue),))
        else:
            self._is_receiving = True

    def stop_rx(self):
        if not self._is_receiving:
            logging.warning('Node %i is expected to be receiving, but is not' % (self.node_id,))
        else:
            self._is_receiving = False

        if len(self._rx_queue) > 0:
            logging.debug('Node %i still had packets in its Rx queue when stopping Rx' % (self.node_id,))
            self.clear_rx_queue(hard=True)

    def rx(self, packet, activate=False):
        if self._is_receiving:
            self._rx_queue.append(packet)

            if activate:
                if self._state == self.State.STAG_WAKEUP and packet.type == EBCPacket.Type.WAKEUP:
                    logging.debug('Node %i activated following a wake-up message from node %i at time %ius' % (self.node_id, packet.sender.node_id, self._sim.get_time(),))
                    self.activate()
                else:
                    logging.debug('Node %i ignored a received activation packet of type %s while in state %s at time %ius' % (self.node_id, packet.type.name, self._state.name, self._sim.get_time(),))

    def clear_rx_queue(self, hard=False):
        if hard:
            # Completely empty Rx queue, including packets received in the current simulation step
            self._rx_queue.clear()

            # Reset exchange time
            self._curr_exchange_time = 0
        else:
            # Remove all old packets, but keep the ones received in the current simulation step
            new_packets = []

            for packet in self._rx_queue:
                if packet.timestamp >= self._sim.get_time():
                    new_packets.append(packet)

            # Overwrite Rx queue
            self._rx_queue = new_packets

    def update_reported_evts(self, report_all=False):
        if len(self._events_reported) > 0:
            logging.warning('Expected reported event list to be empty, but contained %i elements' % (len(self._events_reported),))
            return

        remaining_evts = []
        for event in self.events:
            if report_all:
                # Add all events to the queue
                self._events_reported.append(event)
            elif     self._report_codets_only and len(self._events_reported) == 0:
                # Add the first event to the queue which originally triggered the ad-hoc network
                self._events_reported.append(event)
            elif not self._report_codets_only and event.get_active_time() > self._sample_duration_ms:
                # If event has been sampled long enough, we can send it off
                self._events_reported.append(event)
            else:
                # Add to event queue which might get transmitted as part of the AD phase
                remaining_evts.append(event)

        if len(self.events) != len(self._events_reported) + len(remaining_evts):
            logging.error('Expected to split events into two separate queues, but corrupted data')
        else:
            self.events = remaining_evts
            logging.debug('Node %i added %i events for reporting, %i events remain locally at time %ius' % (self.node_id, len(self._events_reported), len(self.events), self._sim.get_time(),))

    def clear_reported_evts(self, reported):
        if len(self._events_reported) == 0:
            return
        elif reported:
            logging.debug('Node %i removed %i reported events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))
        else:
            logging.debug('Node %i removed %i suppressed events from its queue at time %ius' % (self.node_id, len(self._events_reported), self._sim.get_time(),))

        [event.terminate(reported) for event in self._events_reported]
        self._events_reported.clear()

    def send_discosync(self):
        if not self._enable_discosync:
            # Never send DiscoSync if not enabled
            return False
        else:
            # If have not sent it ourselves yet, send it
            return sum([(packet.type == EBCPacket.Type.DISCOSYNC and packet.sender.node_id == self.node_id) for packet in self._rx_queue]) == 0

    def get_nr_wakeup_messages(self):
        return sum([packet.type == EBCPacket.Type.WAKEUP for packet in self._rx_queue])

    def get_nr_discosync_messages(self):
        return sum([packet.type == EBCPacket.Type.DISCOSYNC for packet in self._rx_queue])

    def get_nr_ebc_events(self, time):
        return sum([packet.nr_events if (packet.type == EBCPacket.Type.DATA and packet.timestamp == time) else 0 for packet in self._rx_queue])


class EBCPacket:

    class Type(Enum):
        INVALID         = 0,
        WAKEUP          = 1,
        DISCOSYNC       = 2,
        LEADER_ELECTION = 3,
        DATA            = 4

    def __init__(self, sender, packet_type, leader=None, reporter=None, nr_events=0):
        self.sender    = sender
        self.type      = packet_type
        self.timestamp = self.sender._sim.get_time()

        self.leader    = leader
        self.reporter  = reporter
        self.nr_events = nr_events

        # Check packet
        if   self.type == EBCPacket.Type.INVALID:
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == EBCPacket.Type.WAKEUP          and (self.leader is not None or self.reporter is not None or nr_events > 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == EBCPacket.Type.DISCOSYNC       and (self.leader is not None or self.reporter is not None or nr_events > 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == EBCPacket.Type.LEADER_ELECTION and (self.leader is not None or self.reporter is not None or nr_events > 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
        elif self.type == EBCPacket.Type.DATA            and (nr_events == 0):
            logging.error('Invalid packet of type %s transmitted by node %i at time %ius' % (self.type.name, self.sender.node_id, self.timestamp,))
