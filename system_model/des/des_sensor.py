#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
"""

# Discrete Event Simulation of Geophone Sensor for the STeC project
#
# Author: abiri
# Date:   03.11.21
#
# This code uses salabim (http://www.salabim.org/manual/index.html) as a DES

import logging
import salabim as sim
from math import ceil, floor, log2

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# We simulate with us granularity
_US      = 1
MS_TO_US = 1000
S_TO_MS  = 1000
S_TO_US  = 1000 * 1000

# Global constants
CHUNK_PER_S   = 1
BAT_VOLTAGE_V = 3.65

MAX_DELTA_US          = 100 * MS_TO_US
MIN_EVENT_DURATION_US =   2 * S_TO_US
MAX_EVENT_DURATION_US =  15 * S_TO_US

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class GeophoneSensor(sim.Component):

    def __init__(self, simulator, parent_node, enable_lpm_costs=True, *args, **kwargs):
        sim.Component.__init__(self, *args, **kwargs)  # Call super constructor

        self.energy_mJ       = 0
        self.measurements_ms = 0

        self._sim               = simulator
        self._parent_node       = parent_node
        self._event             = None
        self._curr_sample_time  = 0
        self._curr_sample_start = 0
        self._last_sample_end   = self._sim.get_time()
        self._curr_triggered    = False
        self._enable_lpm_costs  = enable_lpm_costs

    def process(self):

        LPM_current_uA = 35.637 if self._enable_lpm_costs else 0
        LPM_mW         = LPM_current_uA / 1000 * BAT_VOLTAGE_V

        # Get event info
        if self._event is None:
            logging.warning('Sensor %i was activated even though there was no event assigned to it' % (self._parent_node.node_id,))
            return
        else:
            if self._event.node_id != self._parent_node.node_id:
                logging.warning('Activated incorrect node with ID %i with an event designated for %i' % (self._parent_node.node_id, self._event.node_id,))

            # Get event info
            self._curr_sample_time  = self.compute_sample_time(self._event.duration_us)
            self._curr_sample_start = self._sim.get_time()

            # Update metrics
            self.energy_mJ += (self._sim.get_time() - self._last_sample_end) / S_TO_US * LPM_mW

            # Activate parent node
            self._parent_node.activate()

            # Execute new sampling duration
            yield self.hold(duration=self._curr_sample_time)

            # Store metrics on current event
            actual_sample_us = self._sim.get_time() - self._curr_sample_start
            self.update_sensor_metrics(actual_sample_us)
            logging.debug('Sensor %i finished sampling of event %2i with duration %8ius (sampled duration %8ius) at time %ius' % (self._parent_node.node_id, self._event.sequence_number(), self._curr_sample_time, actual_sample_us, self._sim.get_time(),))

            # Clear event information
            self._event             = None
            self._curr_sample_time  = 0
            self._curr_sample_start = 0
            self._curr_triggered    = False

            # Prepare sensor for next execution
            self._last_sample_end = self._sim.get_time()

    def abort(self):
        if self.get_remaining_time() > 0:
            logging.debug('Sensor %i is aborting sampling with %ius left at time %ius' % (self._parent_node.node_id, self.get_remaining_time(), self._sim.get_time(),))

            if self.isdata():
                logging.error('Sensor %i tried to abort sampling but was not running at time %ius' % (self._parent_node.node_id, self._sim.get_time(),))
            else:
                self.activate()

    def terminate(self):

        # Include LPM costs since last trigger
        if self._enable_lpm_costs:
            LPM_current_uA = 35.637
            LPM_mW         = LPM_current_uA / 1000 * BAT_VOLTAGE_V

            self.energy_mJ += (self._sim.get_time() - self._last_sample_end) / S_TO_US * LPM_mW

        if not self.isdata():
            self.cancel()

    @staticmethod
    def compute_sample_time(evt_duration_us):
        acq_min_duration_s = 2.18
        sample_duration_us = evt_duration_us

        if evt_duration_us == 0:
            logging.warning('Tried running sensor with sampling duration smaller than minimum; clipping to %4.1s' % (MIN_EVENT_DURATION_US / S_TO_US,))
            sample_duration_us = MIN_EVENT_DURATION_US
        elif evt_duration_us > MAX_EVENT_DURATION_US:
            logging.debug('Tried running sensor with sampling duration larger than maximum; clipping to %4.1s' % (MAX_EVENT_DURATION_US / S_TO_US,))
            sample_duration_us = MAX_EVENT_DURATION_US

        nr_additional_chunks = max(0, ceil(sample_duration_us / S_TO_US - acq_min_duration_s))  # Get number of chunks
        sample_duration_us   = (acq_min_duration_s + nr_additional_chunks / CHUNK_PER_S) * S_TO_US

        # FIXME: Overwrite above computation while making sure that we sample at least for the complete propagation period (due to a small percentage of corrupted start times, the min sample duration of 2.18s is not always given and the next events already arrive earlier, which is why we cannot use the "actual" sample time but are forced to use the event duration directly)
        sample_duration_us = max(evt_duration_us, 500 * MS_TO_US)

        return sample_duration_us

    def update_sensor_metrics(self, sampled_duration_us):
        acq_current_mA = 22.701242  # Good approximation without considering chunks

        acq_min_duration_s = 2.18
        acq_min_mJ         = 180.633348  # Minimal measurement of 2.18s
        acq_chunk_mJ       = 87.107018   # 1 chunk corresponds to 1 second of additional measurements

        if sampled_duration_us < self._curr_sample_time:
            # Sampling got aborted, use mixed energy
            sampling_mW = acq_current_mA * BAT_VOLTAGE_V

            sampled_energy_mJ       = sampled_duration_us / S_TO_US * sampling_mW
            measurement_duration_ms = sampled_duration_us / MS_TO_US
        else:
            # As the Geophone measures in chunks of 1s, we must adjust the sampling duration accordingly
            nr_additional_chunks = max(0, ceil(sampled_duration_us / S_TO_US - acq_min_duration_s))  # Get number of chunks

            sampled_energy_mJ       = acq_min_mJ + nr_additional_chunks * acq_chunk_mJ
            measurement_duration_ms = (acq_min_duration_s + nr_additional_chunks / CHUNK_PER_S) * S_TO_MS

        self.energy_mJ       += sampled_energy_mJ
        self.measurements_ms += measurement_duration_ms

    def assign_event(self, event):
        if self._event is not None:
            logging.warning('Attempted to assign event %2i to sensor %i which already has an active event %2i' % (event.sequence_number(), self._parent_node.node_id, self._event.sequence_number(),))
        elif not (self.ispassive() or self.isdata()):
            logging.warning('Attempted to assign event %2i to sensor %i which was already active' % (event.sequence_number(), self._parent_node.node_id,))
        elif event.node_id is not None and event.node_id != self._parent_node.node_id:
            logging.warning('Attempted to assign event %2i which is assigned to node %i to sensor %i' % (event.sequence_number(), event.node_id, self._parent_node.node_id,))
        else:
            self._event         = event
            self._event.node_id = self._parent_node.node_id

            # Activate sensor
            self.activate()

        # Remove event from event queue
        self._sim.evt_queue_pop(self._event)

    def get_event(self):
        if self._event is None:
            logging.error('Requested event from sensor %i while none was active at time %ius' % (self._parent_node.node_id, self._sim.get_time(),))

        return self._event

    def get_remaining_time(self):
        return self._curr_sample_time - (self._sim.get_time() - self._curr_sample_start)

    def triggered(self):
        if self._curr_triggered:
            # Prevent multiple triggers from the same sensor
            return False
        else:
            # The first time that the sensor is called if it has been triggered at this time instance, the function will return True
            self._curr_triggered = self._curr_sample_start == self._sim.get_time()
            return self._curr_triggered
