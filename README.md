# STeC: Exploiting Spatial and Temporal Correlation for Event-based Communication in WSNs

This repository contains scripts for the analysis and simulation of STeC.

| Folder                                                   | Description               |
|----------------------------------------------------------|---------------------------|
| [*analysis/bin*](./analysis/bin)                         | Contains scripts to schedule, download, analyse and evaluate protocol runs and historic data from the deployments |
| [*analysis/data_management*](./analysis/data_management) | Contains scripts to interface with the ETHZ GSN and pre-process the data for other scripts and notebooks |
| [*analysis/notebooks*](./analysis/notebooks)             | Contains notebooks to visualize data from real protocol runs and historic data from the deployments |
| [*system_model/des*](./system_model/des)                 | Contains the discrete event simulation for all protocols treated in the evaluation as well as the synthetic traces |
| [*system_model/notebooks*](./system_model/notebooks)     | Contains notebooks to visualize data from simulated protocol runs |

*Notice:* Occasionally, STeC is still referred to by its working name _EBC_ in some of the variables in this script. To remain compatible with previously generated files and prevent accidental incompatibilities, these variables have not been altered.

For more information, please visit the [STeC wiki](https://gitlab.ethz.ch/tec/public/stec/stec-wiki).
